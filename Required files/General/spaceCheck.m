% Function to determine if a string containt space
function spaceCheck(strName,nameType)
if ~isempty(regexp(strName,' ','once'))
    error([nameType,' ''',strName,''' contains spaces'])
end