% Function to check the unit in the csv file exported by Matlab
% Only deal with mili unit (torque, voltage, etc...)
% if the unit is mili, output will be multiplied by 1000
% if not, kept unchanged
% By check if the header contains a specified string corresponding to the
% unit
% For e.x: if the head contains mNewtonMeter
% If mNewtonMeter returns 0, otherwise return 1
% Input:
% text: text needed to check if it is contained
% fileName: file to check the header
function y = checkUnit(fileName,strCheck)
y = 1;                                                                      % Default as standard unit
fid = fopen(fileName);                                                      % Open file
data = textscan(fid,'%s','Delimiter','');                                   % Get file content in a cell data
colCheck = data{1};                                                         % Get column in the cell data
header = colCheck{1};                                                       % Get first value (header)
index = strfind(header,strCheck);                                           % Check if header contains strCheck

if ~isempty(index)                                                          % If strCheck is contained, results are multiplied with 1e-3
    y = 1e-3;                            
end