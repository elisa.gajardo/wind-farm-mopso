% Function to retrieve a value from the normalized one
% Input:
% - norVal: Normalized value
% - loB and upB: Lower and upper bounds
% - dis: Discrete step: For ex. variables only receive values: loB + dis, lobB + 2*dis, etc..
%   0: Corresponds to continous type
% Output: Unnormalized value
function y = unNormalized(norVal,domain)
loB = domain(:,1);                                                          % Lower bound
upB = domain(:,2);                                                          % Upper bound
nPoint = size(norVal,2);                                                    % Number of variables and points

y = norVal.*repmat((upB - loB),1,nPoint) + repmat(loB,1,nPoint);            % Do normal for all points

y = forceDomain(y,domain);                                                  % Force into specified domain          
            