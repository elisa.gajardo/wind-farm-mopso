% Function to copy all necessary files to run
function pCodeGen(option)
drive = 'C:\Kien\';
path1 = [drive,'Dropbox\1. Study\6. Matlab\2. User defined\General'];
path2 = [drive,'Dropbox\1. Study\6. Matlab\2. User defined\Optimization\Multiobjective optimization'];
path4 = [drive,'Dropbox\1. Study\6. Matlab\2. User defined\Optimization\Sampling'];
path5 = [drive,'Dropbox\1. Study\6. Matlab\2. User defined\Pareto front'];
copyfile([path2,'/multiopt.fig'])
copyfile([path2,'/multioptCheck.fig'])
copyfile([path2,'/optStart.jpg'])
copyfile([path2,'/optNoSolution.jpg'])
copyfile([path2,'/optResultCheck.jpg'])
copyfile([path2,'/optWaiting.jpg'])
copyfile([path2,'/optSymbol.jpg'])
if strcmpi(option,'pfile')
    rpcode(path1);
    rpcode(path2);
    rpcode(path4);
    rpcode(path5);
elseif strcmpi(option,'mfile')
    p1 = subdir([path1,'\*.m']);
    p5 = subdir([path4,'\*.m']);
    for i = 1:length(p1)
       copyfile(p1(i).name); 
    end
    
    for i = 1:length(p5)
       copyfile(p5(i).name); 
    end
end