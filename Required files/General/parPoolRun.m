% Function to run parpool
% Input
% - n: number of clusters
function parPoolRun(n)
inputRangeCheck(n,1,inf);                                                   % No of workers preferred should be bigger than 1
foo = gcp('nocreate');
if isempty(foo)                                                             % If no parpooling is already running
    if n ~= 1
        myCluster = parcluster('local');
        parpool(min(n,myCluster.NumWorkers))                                % Start parpool with no more than available No of clusters
    end
else
    if n ~= foo.NumWorkers                                                  % If the preferred no of workers different from the current no of workers
        delete(foo);                                                        % Turn off parallel
        if n > 1
            myCluster = parcluster('local');
            parpool(min(n,myCluster.NumWorkers))                            % Start parpool with no more than available No of clusters
        end
    end
end
