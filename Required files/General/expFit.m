% Function to do fitting y = a*x^b
% log(y) = log(a) + b*log(x)
% Input: x and y
% Output: a and b
% rSquare: coefficient of determination
function [a,b,rSquare] = expFit(x,y)
[foo] = polyfit(log(x),log(y),1);                                           % Linear fitting for the first order
b = foo(1);
a = exp(foo(2));

% Reconstructed data
y1 = a*x.^b;

% Coefficient of determination
rSquare = rsquare(y,y1,1);

