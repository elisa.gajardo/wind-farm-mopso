% Function to copy all dependent files to the current folder

function dependenceCopy(fileID)

flist = matlab.codetools.requiredFilesAndProducts(fileID);                   % List of dependencies
for i = 1:length(flist)
   copyfile(flist{i});   
end