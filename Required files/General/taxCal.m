% Function to calculate tax to be paid monthly
% Input:
%   - income: monthly total income in Million VND
%   - selfDeduction: Monthly deduction in Million VND
%   - dependPerson: Monthly deduction for dependence in Million VND

function taxpaid = taxCal(income, selfDeduction, dependPerson)
incomeTax = income - selfDeduction - dependPerson;
rates = [5 10 15 20 25 30 35]/100;
brackets=[0, 5, 10, 18, 32, 52, 80];                                        % Unit: VND Million

bracketRevenues =[0, cumsum( diff(brackets).*rates(1:end-1))];
[~,n] = histc(incomeTax,[brackets,inf]);
taxrate = rates(n);
taxpaid = bracketRevenues(n) + (incomeTax-brackets(n))*taxrate;