% Function to remove all ending zero column in a matrix
% Make sure that all zero columns stay at the end of matrix (in the last
% dimension)
function y = zeroTruncate(x)

col = size(x,2);                                                            % Number of columns in x

columnNorm = diag(sqrt(x'*x));                                              % Norm of all columns in x

nZero = col - nnz(columnNorm);                                              % Number of zero column in x

if nZero > 0
    x(:,col:-1:col-nZero + 1) = [];                                         % Remove all zero column
end

y = x;

if isempty(y)
    y = 0;
end