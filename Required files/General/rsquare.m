% Function to calculate coefficient of determination (R squared) between 2
% curves: y = rsquare(y1,y2,p)
% Input: y1 - observation and y2 needed to be compared with y1
% p: number of predictor variables: p = 1 (Default)
% Output COD: coefficient of determination

function COD = rsquare(y1,y2,p)

SSE = sum((y1-y2).^2);                                                      % Sum of squared error
SST = length(y1)*var(y1,1);                                                 % Sum of squared total

if nargin == 2     
    COD = 1-SSE/SST;                                                        % By default: un-adjusted R square
else
    COD = 1-(length(y1)-1)/(length(y1)-p)*SSE/SST;                          % Adjusted R square
end
% Modification to be normalized between 0 and 1
COD = 1/(2-COD);
% Modification the range
COD = COD^2;