% Function to calculate new root for the next iteration in the fixed-point
% method
% Input
% - xStar: newly recalculated root
% - xOld: root from the previous iteration
% - alpha: relaxation factor between 0 and 1
%   (alpha = 1 i.e directly use newly recalculated root)
% Ouput
% - xNew: updated root for the next iteration
function xNew = FPMupdate(xStar,xOld,alpha)
% Classical algorithm
xNew = xStar*alpha + xOld*(1 - alpha);