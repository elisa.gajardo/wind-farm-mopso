% Function to forecast future time-series data
% Input
% - pastData: Past data
% - timeStep: Step time
% - K: Number of point for future forecasting
% - n: Polynomial order
% - display: 1 if want to plot data
% Output: 
% - Future data
function y = futureForecast(pastData,timeStep,n,K,display)
if size(pastData,2) > size(pastData,1)
    pastData = pastData';                                                   % Convert to column data if row data is used
end
pastData = iddata(pastData,[],timeStep);                                    % Convert to time-series data
sys = ar(pastData,n);                                                       % Forecasting model                    
y = forecast(sys,pastData,K);                                               % Future time-series data

if display
    figure(1)
    plot(pastData,'b',y,'r')
    legend('Past data','Forecasted data')
    grid on
end
end