% Function to check an input number must be in a certain range [a,b]
function inputRangeCheck(n,a,b)
if n < a || n > b
    error(msgbox(['Input must be in a range [',num2str(a),',',num2str(b),']']));
end