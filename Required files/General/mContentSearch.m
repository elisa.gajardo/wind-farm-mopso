% Function to search for .m files consisting a given text
% Input:
% - parentPath: parent path to search, leave empty [] if using current path
% - searchText: text to search
% Output:
% - All full pahts to the searched files

function output = mContentSearch(parentPath, textSearch)
if isempty(parentPath)
    fileList = subdir('*.m');                                               % Recursively search for files with the given extension
else
    fileList = subdir([parentPath,'\*.m']);
end
k = 1;
if ~isempty(fileList)
    for i = 1:length(fileList)
        fileName = fileList(i).name;                                        % Get the full path of the file
        fileContent = fileread(fileName);                                   % Read the file
        strSearch = regexp(fileContent,textSearch,'once');                  % Search for text
        if ~isempty(strSearch)
            output{k} = fileList(i).name;
            k = k + 1;
        end
    end
    if k == 1
        disp('No results found');
        output = [];
    end
else
    output = [];
    disp('No results found')
end
