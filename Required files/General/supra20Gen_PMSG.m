% Function to copy all necessary files to run
function supra20Gen_PMSG()
drive = 'C:\Kien\';
path1 = [drive,'Dropbox\1. Study\12. Supra20\9. Machine design\2. Maxwell model'];
path2 = [drive,'Dropbox\1. Study\12. Supra20\8. Matlab\Generator'];
path3 = [drive,'Dropbox\1. Study\12. Supra20\8. Matlab'];
copyfile([path1,'\surfacePMSG.mxwl'])
copyfile([path1,'\surfacePMSG_noLoad.mxwl'])
rpcode(path1);
rpcode(path2);
rpcode(path3);