% Function to copy all necessary files to run
% Option:
% - pfile: generate p files
% - mfile: copy m files
function multioptFileCollect(fileType)
windowsPath = 'D:\Dropbox\1. Study\6. Matlab\2. User defined';

% Path for files
path1 = [windowsPath,'\General'];
path2 = [windowsPath,'\Optimization/Sampling'];
path3 = [windowsPath,'\Optimization/Multiobjective optimization'];
path4 = [windowsPath,'\Pareto front'];
copyfile([path3,'\multiopt.fig'])

% File type option
if strcmpi(fileType,'pfile')
    rpcode(path1);
    rpcode(path2);
    rpcode(path3);
    rpcode(path4);
elseif strcmpi(fileType,'mfile')
    p1 = subdir([path1,'\*.m']);
    p2 = subdir([path2,'\*.m']);
    p3 = subdir([path3,'\*.m']);
    p4 = subdir([path4,'\*.m']);
    for i = 1:length(p1)
       copyfile(p1(i).name); 
    end
    
    for i = 1:length(p2)
       copyfile(p2(i).name); 
    end
    
    for i = 1:length(p3)
       copyfile(p3(i).name); 
    end
    
    for i = 1:length(p4)
       copyfile(p4(i).name); 
    end
else
    error('Not a correct file type: mfile or pfile only')
end