% Function to determine if a value belong to periods determined by:
% (a(1,1) a(1,2)];
% (a(2,2) a(2,2)]); etc...
function y = zoneDetermine(x,a)
y = 0; % Dont belong to is default
temp = x*ones(size(a,1),2) - a;
if prod(temp(:,1).*temp(:,2))<0;
    y = 1;
end
if prod(temp(:,2)) == 0;
    y = 1;
end