% Function to calcualte the ripple of a waveform
% Input
% - signal
function y = ripple(signal)
if mean(signal) == 0
    error('Mean value is zero')
else
    y = 0.5*(max(signal) - min(signal))/mean(signal)*100;
end