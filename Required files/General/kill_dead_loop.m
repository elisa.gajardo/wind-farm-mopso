function kill_dead_loop()
path = pwd;
% max time limit for the running of a function. You can change the value
% below
max_time_running = 10;
%
m = 1;
while m<5000
    %
    processName = 'solver2D'; % for example
    p = System.Diagnostics.Process.GetProcessesByName(processName);
    if p.Length == 1
        pid_ini = p(1).Id; % You must index into p (not p.Id), as this changes the class type
    end
    %
    % ! matlab -nojvm -nodesktop -nosplash -r Monte_Carlo_Simulations &
    %     ! matlab -nodesktop -nosplash -r dummy_dead_loop &
    pause(2)
    %
    tic;
    %
    %
    % matlab_name = 'matlab'; % for example
    p = System.Diagnostics.Process.GetProcessesByName(processName);
    %
    pid_master = p(1).Id; % You must index into p (not p.Id), as this changes the class type
    pid_slave  = p(2).Id; % You must index into p (not p.Id), as this changes the class type
    if pid_slave == pid_ini
        pid_temp = pid_master;
        pid_master = pid_slave;
        pid_slave  = pid_temp;
    end
    %
    flag = 1;
    %
    while flag > 0
        end_time = toc;
        %
        if end_time > max_time_running
            eval(['!',' ','taskkill',' ','-f',' ','-im',' ',num2str(pid_slave)])
            flag = 0;
        end
    end
    m = m+1;
end