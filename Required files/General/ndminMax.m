% Function to find minimum value in a multi-dimensional matrix
% Option
% - min, max: to find min or max value correspondingly
function [optValue, optIndex, subIndex] = ndminMax(A,option)
if strcmpi(option,'min')
    [optValue, optIndex] = min(A(:));                                       % Find the minimum element in A
elseif strcmpi(option,'max')
    [optValue, optIndex] = max(A(:));                                       % Find the maximum element in A
else
    error('Only min or max option allowed')
end

% Convert from index to subscripts
if ndims(A) == 2                                                            % If A is a matrix
    [subIndex(1), subIndex(2)] = ind2sub(size(A), optIndex);
elseif ndims(A) == 3                                                        % 3 dimensional array
    [subIndex(1), subIndex(2), subIndex(3)] = ind2sub(size(A), optIndex);
elseif ndims(A) == 4                                                        % 4 dimensional array
    [subIndex(1), subIndex(2), subIndex(3), subIndex(4)] = ind2sub(size(A), optIndex);
else
    error('Array with more than 4 dimension is not supported')
end