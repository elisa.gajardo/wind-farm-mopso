% Function to get internet month and year
function [currentYear,currentMonth,currentDay] = getCurrentTime()
% try
%     URL = 'http://tycho.usno.navy.mil/cgi-bin/timer.pl';
%     atomTime = datenum(regexp(urlread(URL), ...
%         '<BR>(.*)\sUTC','tokens','once'),'mmm. dd, HH:MM:SS');
% catch
%     atomTime = now;
% end
atomTime = now;

V = datevec(atomTime);

currentYear = V(1);
currentMonth = V(2);
currentDay = V(3);