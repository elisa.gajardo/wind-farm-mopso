% Function to generate initial start of optimization process
% Input
% - domain: design space
% - nPoint: number of initial points
% - method: sampling technique: LHS, standard uniform distribution (SUD)
function y = x0Gen(domain,nPoint,method)
nVar = size(domain,1);                                                      % Number of variables
switch upper(method)
    case 'SUD'
        y = repmat(domain(:,1),1,nPoint) + repmat(domain(:,2)-domain(:,1),1,nPoint).*rand(nVar,nPoint);
        y = forceDomain(y,domain);
    case 'LHS'
        yNormalized = lhsdesign(nPoint,nVar)';
        y = unNormalized(yNormalized,domain);
end