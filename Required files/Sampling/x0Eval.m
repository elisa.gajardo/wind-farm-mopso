% Function to generate x0 by random process until a feasible solution found
% Or evaluate a set of point to find feasible solutions
% Verify both nonlinear and linear inequality (Ax <= B)
% Input
% - domain: design domain
% - A, B: linear inequality coefficient constraint
% - maxTrial (either a maximum number of point wanna be generated or name
% of set point wanna be evaluated
% - funName: function name to calculate constraint (as usual, both OBJ and
% CONSTR)
% - runningMode: 'parallel' or 'series'
% - UserInput: additional parameter as defined by user
% - messageDisplay: display message if no feasible solution found
% P/S:  with series type: step by step until a feasible solutions found
%       with parallel type: has to evaluate in parallel all point
function x0 = x0Eval(domain,A,B,maxTrial,funName,runningMode,UserInput, messageDisplay)

if isnumeric(maxTrial)
    trialSet = x0Gen(domain,maxTrial,'lhs');
else
    load(maxTrial);
    trialSet = eval(maxTrial);
end

if isempty(A)
    A = zeros(1,size(domain,1));
    B = 0;
end

if strcmpi(runningMode,'parallel')
    parfor i = 1:size(trialSet,2)
        [objective ,constraint] = feval(funName,trialSet(:,i),UserInput);   % Evaluate the trial set
            constraint = [constraint; A*trialSet(:,i) - B];                 % Add linear inequality to the total constraint set
            OBJ(i) = objective;                                             % Store all objectives
            CONSTRcheck(i) = all(constraint <= 0);
    end
    
    % Search for feasible set
    bestOBJ = max(OBJ(CONSTRcheck == 1));
    x0 = trialSet(:,CONSTRcheck == 1 & OBJ == bestOBJ);
    
    % Find the best point in the feasible set
%     x0 = x0(:,randi(size(x0,2)));                                           % Choose a random solution from the feasible set
    
elseif strcmpi(runningMode,'series')
    for i = 1:size(trialSet,2)
        [~,constraint] = feval(funName,trialSet(:,i),UserInput);
        constraint = [constraint; A*trialSet(:,i) - B];                     % Add linear inequality to the total constraint set
        if all(constraint <= 0)
            x0 = trialSet(:,i);
            break
        end
    end
end

if ~exist('x0','var') || isempty(x0)
    if messageDisplay
        display('No feasible solution to start')
    end
    x0 = zeros(size(domain,1),0);                                           % Create empty starting point
end