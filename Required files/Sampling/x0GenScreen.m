% Function to generate initial start of optimization process
% Input
% - domain: design space
% - nPoint: number of initial points
% - varOrder: order of the variable (to be screened) in the variable list
function y = x0GenScreen(domain,nPoint,varOrder)
nVar = size(domain,1);                                                      % Number of variables

% Determine values of variable to be screened
varScreen = linspace(domain(varOrder,1),domain(varOrder,2),nPoint);

% Determine all variables which are not screened
for i = 1:nPoint
    for j = 1:nVar
        if j ~= varOrder
            y(j,i) = (domain(j,2) + domain(j,1))/2;                         % Take the value in between the lower and upper bounds
        else
            y(j,i) = varScreen(i);
        end
    end
end

