% Function to assign variables from a known set
% It has to fit the known set to the pre-defined variable set (number of
% points)
% Input:
% - x0: sample point to assign
% - nPoint: number of point in the predefined variable set
% - domain: variable domain in case wanna generate random points
% For e.x: it is required to have a set of 3 point:
% The known set is [1 2 3 4;5 6 7 8] --> generate [1 2 3;5 6 7]
% The known set is [1 2;5 6] --> generate [1 2 1;5 6 5]
% Two methods to be used:
% - Method 1: only use x0
% - Method 2: if no of desized points > n0 of point in the x0 set
% x0 will be used for the first set, then the rest is randomly generated
function variable = x0Assign(x0,nPoint,domain)
m = size(x0,2);                                                             % Number of point in the x0 set
if nargin == 2
    if nPoint <= m
        variable = x0(:,1:nPoint);
    else
        a = mod(nPoint,m);
        b = (nPoint - a)/m;
        variable = repmat(x0,1,b);                                          % Repeat b times set x0
        variable = [variable, x0(:,1:a)];                                   % Add 'a' first component of x0
    end
elseif nargin == 3
    if nPoint <= m
        variable = x0(:,1:nPoint);
    else
        varFirst = x0;                                                      % x0 is applied for the first set
        varRest = x0Gen(domain,nPoint - m,'lhs');                           % Random generation for the rest
        variable = [varFirst varRest];
    end
end