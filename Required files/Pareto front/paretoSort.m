% This function is to sort the non-dominated front on the increasing
% of 1st objective (note: objectives are assumed to be minimized)
% Input: objective and maybe with design vector
% If no design variable vector presented, only sort the pareto front based on the first objective.
function [xSort,ySort,zSort] = paretoSort(xSample,ySample,zSample)
if ~isempty(xSample)
    n = size(xSample,2);                                                    % Number of sample points
    for i = 1:n
        [~,index] = min(xSample(1,:));                                      % Find minimum value in the first objective dimension
        xSort(:,i) = xSample(:,index);                                     	% Extract the point with that index in current iteration
        xSample(:,index) = [];                                              % Remove that index from the sample list
        ySort(:,i) = ySample(:,index);
        ySample(:,index) = [];
        if nargin == 3
            zSort(:,i) = zSample(:,index);
            zSample(:,index) = [];
        end
    end
else
    xSort = [];
    ySort = [];
end