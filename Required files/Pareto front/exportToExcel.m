% Function to export pareto front to excell
function exportToExcel(OBJpareto,VARpareto,CONSTRpareto,MISCpareto,fileExport)
rounding = 1e3;
warning('off','MATLAB:xlswrite:AddSheet');
if isempty(OBJpareto)
    uiwait(msgbox('Pareto front is empty. No result is exported to Excel','Warning'))
else
    fileExport = [fileExport,'.xlsx'];
    if exist(fileExport,'file')                                             % Delete fileExport if already existed
        delete(fileExport);
    end
    maxCol = 15;                                                            % Maximum number of columns in excel for result
    VARpareto = VARpareto(:,:,end);                                         % Variables
    OBJpareto = OBJpareto(:,:,end);                                         % Objectives
    CONSTRpareto = CONSTRpareto(:,:,end);
    MISCpareto = MISCpareto(:,:,end);
    
    VARpareto = ceil(VARpareto*rounding)/rounding;
    OBJpareto = ceil(OBJpareto*rounding)/rounding;
    CONSTRpareto = ceil(CONSTRpareto*rounding)/rounding;
    MISCpareto = ceil(MISCpareto*rounding)/rounding;
    
    [nVar, nPoint] = size(VARpareto);                                       % Number of variables and points on pareto front
    nOBJ = size(OBJpareto,1);                                               % Number of objectives
    nCONSTR = size(CONSTRpareto,1);                                         % Number of constraints
    nMISC = size(MISCpareto,1);                                             % Number of misc.
    
    for i = 1:nPoint
        COLheader{1,i} = ['Point ',num2str(i)];
    end
    
    for i = 1:nVar
        VARrow{i,1} = ['x',num2str(i)];
    end
    
    for i = 1:nOBJ
        OBJrow{i,1} = ['Objective ',num2str(i)];
    end
    
    for i = 1:nCONSTR
        CONSTRrow{i,1} = ['Constraint ',num2str(i)];
    end
    
    for i = 1:nMISC
        MISCrow{i,1} = ['Misc. ',num2str(i)];
    end
    
    
    % Variables
    for i = 1:nVar
        for j = 1:nPoint
            VARexport{i,j} = VARpareto(i,j);
        end
    end
    % Objective
    for i = 1:nOBJ
        for j = 1:nPoint
            OBJexport{i,j} = OBJpareto(i,j);
        end
    end
    % Constraint
    for i = 1:nCONSTR
        for j = 1:nPoint
            CONSTRexport{i,j} = CONSTRpareto(i,j);
        end
    end
    % Misc.
    for i = 1:nMISC
        for j = 1:nPoint
            MISCexport{i,j} = MISCpareto(i,j);
        end
    end
    
    
    nGroup = (nPoint - mod(nPoint,maxCol))/maxCol;
    
    % Number of groups (each group consistts of maxCol columns)
    for i = 1:nGroup
        VARgroup(:,:,i) = VARexport(:,maxCol*(i - 1) + 1:maxCol*i);
        OBJgroup(:,:,i) = OBJexport(:,maxCol*(i - 1) + 1:maxCol*i);
        CONSTRgroup(:,:,i) = CONSTRexport(:,maxCol*(i - 1) + 1:maxCol*i);
        MISCgroup(:,:,i) = MISCexport(:,maxCol*(i - 1) + 1:maxCol*i);
    end
    VARgroupEnd = VARexport(:,maxCol*nGroup + 1:end);
    OBJgroupEnd = OBJexport(:,maxCol*nGroup + 1:end);
    CONSTRgroupEnd = CONSTRexport(:,maxCol*nGroup + 1:end);
    MISCgroupEnd = MISCexport(:,maxCol*nGroup + 1:end);
    
    distance = (nVar + 2)*(nGroup + 1) + 3;                                 % Distance between variable and objective areas
    
    % Writing variables
    for i = 1:nGroup
        % Write header (point)
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:maxCol*i),'Pareto front',['C',num2str(i + 2 + (nVar + 1)*(i - 1))]);
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:maxCol*i),'Pareto front',['C',num2str(i + 2 + (nOBJ + 1)*(i - 1) + distance)]);
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:maxCol*i),'Constraint',['C',num2str(i + 2 + (nCONSTR + 1)*(i - 1))]);
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:maxCol*i),'Misc.',['C',num2str(i + 2 + (nMISC + 1)*(i - 1))]);
        % Write title
        xlswrite(fileExport,VARrow,'Pareto front',['B',num2str(i + 3 + (nVar + 1)*(i - 1))]);
        xlswrite(fileExport,OBJrow,'Pareto front',['B',num2str(i + 3 + (nOBJ + 1)*(i - 1)+ distance)]);
        xlswrite(fileExport,CONSTRrow,'Constraint',['B',num2str(i + 3 + (nCONSTR + 1)*(i - 1))]);
        xlswrite(fileExport,MISCrow,'Misc.',['B',num2str(i + 3 + (nMISC + 1)*(i - 1))]);
        % Write data
        xlswrite(fileExport,VARgroup(:,:,i),'Pareto front',['C',num2str(i + 3 + (nVar + 1)*(i - 1))]);
        xlswrite(fileExport,OBJgroup(:,:,i),'Pareto front',['C',num2str(i + 3 + (nOBJ + 1)*(i - 1)+ distance)]);
        xlswrite(fileExport,CONSTRgroup(:,:,i),'Constraint',['C',num2str(i + 3 + (nCONSTR + 1)*(i - 1))]);
        xlswrite(fileExport,MISCgroup(:,:,i),'Misc.',['C',num2str(i + 3 + (nMISC + 1)*(i - 1))]);
    end
    
    i = nGroup + 1;
    if mod(nPoint,maxCol)
        % Write header (point)
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:end),'Pareto front',['C',num2str(i + 2 + (nVar + 1)*(i - 1))]);
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:end),'Pareto front',['C',num2str(i + 2 + (nOBJ + 1)*(i - 1)+ distance)]);
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:end),'Constraint',['C',num2str(i + 2 + (nCONSTR + 1)*(i - 1))]);
        xlswrite(fileExport,COLheader(maxCol*(i-1) + 1:end),'Misc.',['C',num2str(i + 2 + (nMISC + 1)*(i - 1))]);
        % Write title
        xlswrite(fileExport,VARrow,'Pareto front',['B',num2str(i + 3 + (nVar + 1)*(i - 1))]);
        xlswrite(fileExport,OBJrow,'Pareto front',['B',num2str(i + 3 + (nOBJ + 1)*(i - 1)+ distance)]);
        xlswrite(fileExport,CONSTRrow,'Constraint',['B',num2str(i + 3 + (nCONSTR + 1)*(i - 1))]);
        xlswrite(fileExport,MISCrow,'Misc.',['B',num2str(i + 3 + (nMISC + 1)*(i - 1))]);
        % Write data
        xlswrite(fileExport,VARgroupEnd,'Pareto front',['C',num2str(i + 3 + (nVar + 1)*(i - 1))]);
        xlswrite(fileExport,OBJgroupEnd,'Pareto front',['C',num2str(i + 3 + (nOBJ + 1)*(i - 1)+ distance)]);
        xlswrite(fileExport,CONSTRgroupEnd,'Constraint',['C',num2str(i + 3 + (nCONSTR + 1)*(i - 1))]);
        xlswrite(fileExport,MISCgroupEnd,'Misc.',['C',num2str(i + 3 + (nMISC + 1)*(i - 1))]);
    end
end

