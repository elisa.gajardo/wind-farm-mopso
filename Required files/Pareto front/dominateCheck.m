% Function to check if one point dominate other or non-dominate
% Input:    point1, point2 (with n dimensions)
% Output: 	0: if no one dominate other
%         	1: if point 1 dominate point 2
%          	-1: if point 2 dominate point 1
function y = dominateCheck(point1,point2)
n = length(point1);
lessCount = 0;                                                              % Number of components in point 1 smaller than corresponding in point 2
equalCount = 0;                                                          	% Number of components in point 1 equals corresponding in point 2
biggerCount = 0;                                                            % Number of components in point 1 bigger than corresponding in point 2
for i = 1:n
    if point1(i) < point2(i)
        lessCount = lessCount + 1;
    elseif point1(i) == point2(i)
        equalCount = equalCount + 1;
    else
        biggerCount = biggerCount + 1;
    end
end

if ((lessCount > 0) && (lessCount + equalCount == n))
    y = 1;
elseif ((lessCount > 0) && (biggerCount > 0))
    y = 0;
else
    y = -1;
end
    