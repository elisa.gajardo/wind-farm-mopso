function objectiveDisplay(Objective,OBJviolate,OBJpareto)
symbol = 'ob';
OBJcolor = 'b';
h1 = {'All solution','Violated solution','Pareto front'};
h2 = {'All solution','Violated solution'};
h3 = {'All solution','Pareto front'};

if nargin == 3
    OBJcolor = 'y';
    if isempty(OBJviolate)
        h = h3;
    elseif isempty(OBJpareto)
        h = h2;
    else h = h1;
    end
end

if size(Objective,1) == 2                                                   % If there are only 2 objectives
    plot(Objective(1,:),Objective(2,:),symbol,'markersize',6,'markerfacecolor',OBJcolor);
    hold on
    if nargin == 3
        if ~isempty(OBJviolate)
            plot(OBJviolate(1,:),OBJviolate(2,:),symbol,'markersize',6,'markerfacecolor','r');
        end
        if ~isempty(OBJpareto)
            plot(OBJpareto(1,:),OBJpareto(2,:),symbol,'markersize',6,'markerfacecolor','b');
        end
        legend(h);
    end
    hold off
    grid
    xlabel('Objective 1')
    ylabel('Objective 2')
else
    if nargin == 1
        if size(Objective,2) >= 2                                           % If there are more than 2 nondominated solutions
            paretoSurface(Objective(1,:),Objective(2,:),Objective(3,:));    % Plot approximated surface
        else                                                                
            plot3(Objective(1,:),Objective(2,:),Objective(3,:),symbol,'markerfacecolor',OBJcolor);  % Just plot discrete points
        end
    elseif nargin == 3
        plot3(Objective(1,:),Objective(2,:),Objective(3,:),symbol,'markerfacecolor',OBJcolor);
        hold on
        if ~isempty(OBJviolate)
            plot3(OBJviolate(1,:),OBJviolate(2,:),OBJviolate(3,:),symbol,'markersize',6,'markerfacecolor','r');
        end
        if ~isempty(OBJpareto)
            plot3(OBJpareto(1,:),OBJpareto(2,:),OBJpareto(3,:),symbol,'markersize',6,'markerfacecolor','b');
        end
        legend(h)
    else
    end
    hold off
    grid on
    xlabel('Objective 1')
    ylabel('Objective 2')
    zlabel('Objective 3')
end
drawnow