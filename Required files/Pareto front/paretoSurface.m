function paretoSurface(xData, yData, zData)

% Fit: 'untitled fit 1'.
[xData_1, yData_1, zData_1] = prepareSurfaceData(xData,yData,zData );

% Set up fittype and options.
ft = 'biharmonicinterp';

% Fit model to data.
fitresult = fit([xData_1, yData_1], zData_1, ft);

% Plot fit with data.
plot(fitresult,[xData_1, yData_1], zData_1);
hold on
plot3(xData,yData,zData,'ob','markerfacecolor','m');