function objDisplayShort()
uiopen('load')
nIter = size(OBJpareto,3);                                                  % Number of iterations

if size(OBJpareto,1) == 2                                                   % If there are only 2 objectives
    figure(111)
    clf
    OBJtemp = OBJpareto(:,:,end);
    if OBJtemp(1) ~= OBJtemp(2)
        hold on
        for i = 1:nIter
            OBJi = zeroTruncate(OBJpareto(:,:,i));
            if i < nIter
                plot(OBJi(1,:),OBJi(2,:),'*','markersize',6,'color',[0.5 0.5 0.5]);
            else
                plot(OBJi(1,:),OBJi(2,:),'b^','markersize',6,'markerfacecolor','b');
            end
        end
        hold off
        grid
        xlabel('Objective 1')
        ylabel('Objective 2')
    else
        hold on
        for i = 1:nIter
            OBJi = OBJpareto(:,:,i);
            if i < nIter
                plot(i,OBJi(1),'-*','markersize',6,'color',[0.5 0.5 0.5]);
            else
                plot(i,OBJi(1),'b>','markersize',9,'markerfacecolor','b');
            end
        end
        hold off
        grid
        xlabel('Iteration')
        ylabel('Objective')
    end
end
drawnow