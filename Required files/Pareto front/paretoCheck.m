% This function is to determine a set of point on an existing pareto front
% dominated by a new point. This will divide the existing pareto front into
% 2 sub-group if the new point dominates any existing point
% input:
% - A pareto front
% - A new point
% Output
% - New pareto front considering new point
% - Set of points dominated by the new point
% - domCheck: Check if all points on the current pareto dominates the trial point
function [dominatedSet,domCheck] = paretoCheck(currentPareto,newPoint)
n = size(currentPareto,2);
dominatedSet = [];                                                          % Initialize set dominated by the trial point
nDom = 0;                                                                   % Initialize number of points on the current pareto dominated the trial point
for i = 1:n
    temp = dominateCheck(newPoint,currentPareto(:,i));
    if temp == 1                                                            % If newPoint dominates the point being checked
        dominatedSet = [dominatedSet; currentPareto(:,i)];
    elseif temp == -1                                                       % If the point being checked dominates newPoint
        nDom = nDom + 1;
    end
end
if nDom >= 1
    domCheck = 1;                                                           % If exist 1 point on the current pareto dominate the trial point (i.e. can not improve the current pareto front)
else
    domCheck = 0;                                                           % If not
end