% Function to check if there is a feasible solution in the constraint space

function y = feasibleCheck(constraint)

y = 0;

for i = 1:size(constraint,2)
    if all(constraint(:,i) <= 0)
        y = 1;
    end
end