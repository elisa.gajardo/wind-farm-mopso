clc
saveFile = 'Result_MOPSO';                                                  % Result file name
startTime = datestr(datetime('now'));
fid = fopen([saveFile,'.txt'],'w');
fprintf('Program has been launched at:\n');
fprintf('\n');
fprintf('%s ... >>\n',startTime);
fprintf('\n');
splitLine = repmat('-',1,49);
fprintf('%s\n',splitLine);
fprintf('|  %10s  |  %8s  |  %11s  |\n','Generation','Solution','Duration (mins)');
fprintf('%s\n',splitLine);

% Write to file
fprintf(fid,'Program has been launched at:\n');
fprintf(fid,'\n');
fprintf(fid,'%s ... >>\n',startTime);
fprintf(fid,'\n');
splitLine = repmat('-',1,49);
fprintf(fid,'%s\n',splitLine);
fprintf(fid,'|  %10s  |  %8s  |  %11s  |\n','Generation','Solution','Duration (mins)');
fprintf(fid,'%s\n',splitLine);