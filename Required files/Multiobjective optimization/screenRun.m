% Function to do screening with specified variables to be screened

function [Objective,OBJviolate,OBJpareto,VARpareto] = screenRun(Option)
% Initialisation du generateur aleatoire
rand('state',sum(100*clock));

N_runs = Option.nIndividual;

set(0,'RecursionLimit',100)

% Objective function parameters
fonction = Option.Fcn;
domain = Option.domain;

% Get variable to be screened
varScreen = Option.domain(:,4);                                             % 4th column of the domain where screening options are stored

if nnz(varScreen) == 0
    msgbox('No variables are chosen to screen','Warning')
end

for i = 1:length(varScreen)
    if varScreen(i) == 1
        % Saving parameter
        saveFile = ['Result_screening_',num2str(i)];                        % Result file name
        
        if exist([saveFile,'.mat'],'file')
            delete([saveFile,'.mat'])                                       % Delete result file if existed
        end
        
        % First trial generation
        Variable = x0GenScreen(domain,N_runs,i);
        % Effet quantique pour les variables discretes
        Variable = forceDomain(Variable,domain);
        % Evaluation of trials
        [Objective,Constraint,Misc] = feval(fonction,Variable);
        Somme = sum(max(Constraint,0),1);
        indexPos = find(Somme > 0);
        OBJviolate = Objective(:,indexPos);
        OBJpareto = Objective;
        VARpareto = Variable;
        CONSTRpareto = Constraint;
        MISCpareto = Misc;
        
        OBJpareto(:,indexPos) = [];
        VARpareto(:,indexPos) = [];
        CONSTRpareto(:,indexPos) = [];
        MISCpareto(:,indexPos) = [];
        
        if ~isempty(OBJpareto)
            [~,~,MISCpareto] = paretoFront(OBJpareto,0,VARpareto,MISCpareto);
            [OBJpareto,VARpareto,CONSTRpareto] = paretoFront(OBJpareto,0,VARpareto,CONSTRpareto);
            
            save(saveFile,'OBJpareto','VARpareto','CONSTRpareto')
            if Option.plotDisplay
                if nnz(varScreen) > 1                                          
                    figure()                                                % Pop out a new figure if there are more than one screening variables
                end
                objectiveDisplay(Objective,OBJviolate,OBJpareto);
                title(['Screening check for variable ',num2str(i)]);
            end
        else
            OBJpareto = Objective;
            VARpareto = Variable;
            CONSTRpareto = Constraint;
            MISCpareto = Misc;
            NotePareto = 'All points are unfeasible';
            save(saveFile,'OBJpareto','VARpareto','CONSTRpareto','MISCpareto','NotePareto')
            if Option.plotDisplay
                if nnz(varScreen) > 1
                    figure()                                                % Pop out a new figure if there are more than one screening variables
                end
                objectiveDisplay(Objective,OBJviolate,[]);
                title(['Screening check for variable ',num2str(i)]);
            end
            display('No feasible solution found')
        end
        if Option.exportToExcel
            exportToExcel(OBJpareto,VARpareto,CONSTRpareto,MISCpareto,saveFile);
        end
    end
end
