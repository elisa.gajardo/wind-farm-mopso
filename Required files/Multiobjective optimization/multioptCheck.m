function varargout = multioptCheck(varargin)
% MULTIOPTCHECK MATLAB code for multioptCheck.fig
%      MULTIOPTCHECK, by itself, creates a new MULTIOPTCHECK or raises the existing
%      singleton*.
%
%      H = MULTIOPTCHECK returns the handle to a new MULTIOPTCHECK or the handle to
%      the existing singleton*.
%
%      MULTIOPTCHECK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MULTIOPTCHECK.M with the given input arguments.
%
%      MULTIOPTCHECK('Property','Value',...) creates a new MULTIOPTCHECK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before multioptCheck_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to multioptCheck_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help multioptCheck

% Last Modified by GUIDE v2.5 13-Jun-2017 13:11:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @multioptCheck_OpeningFcn, ...
    'gui_OutputFcn',  @multioptCheck_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before multioptCheck is made visible.
function multioptCheck_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to multioptCheck (see VARARGIN)

% Choose default command line output for multioptCheck
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
try
    imshow(imread('optResultCheck.jpg'));
end

% UIWAIT makes multioptCheck wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = multioptCheck_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in browseFile.
function browseFile_Callback(hObject, eventdata, handles)
% hObject    handle to browseFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fileName,filePath] = uigetfile({'*.*','All Files'});
if filePath
    cd(filePath);
    handles.browseFile = load([filePath fileName]);
    guidata(hObject, handles);
end

% --- Executes on selection change in checkList.
function checkList_Callback(hObject, eventdata, handles)
% hObject    handle to checkList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns checkList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from checkList
optCheck = get(hObject,'Value');
greyCode = 0.863*ones(1,3);
set(handles.saveMat,'Visible','off')
switch optCheck
    case 1
        set(handles.saveMat,'Visible','off')
    case 2
        set(handles.saveMat,'Visible','off')
    case 3
        set(handles.saveMat,'Visible','off')
    case 4
        set(handles.saveMat,'Visible','on')
    case 5
        set(handles.saveMat,'Visible','on')
end
        

% --- Executes during object creation, after setting all properties.
function checkList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figRotate,'Visible','off')
rotate3d off

saveMatValue = get(handles.saveMat,'Value');
startAction = get(handles.checkList,'Value');
resultAll = handles.browseFile;
OBJpareto = resultAll.OBJpareto;
VARpareto = resultAll.VARpareto;
CONSTRpareto = resultAll.CONSTRpareto;
MISCpareto = resultAll.MISCpareto;

lastParetoObj = resultAll.lastParetoObj;
lastParetoVar = resultAll.lastParetoVar;
lastParetoMisc = resultAll.lastParetoMisc;
multioptOption = resultAll.Option;

nOBJ = size(OBJpareto,1);
switch startAction
    
    %--------------------------------------------
    % Pareto display
    %--------------------------------------------
    case 1
        objectiveDisplay(lastParetoObj)
        [a,~] = view;
        if a ~= 0
            set(handles.figRotate,'Visible','on')
        end
    case 2
        prompt = {'Excel file name to export'};
        dlg_title = '';
        num_lines = 1;
        defaultans = {'Result_'};
        excelFileName = inputdlg(prompt,dlg_title,num_lines,defaultans);
        if ~isempty(excelFileName)
            exportToExcel(OBJpareto,VARpareto,CONSTRpareto,MISCpareto,excelFileName{1});
        else
            errordlg('Empty file name','Error')
        end
        
    %--------------------------------------------
    % Pareto evolution
    %--------------------------------------------
    case 3
        nIter = size(OBJpareto,3);                                          % Number of iterations
        if nOBJ == 2                                                        % If there are only 2 objectives
            OBJtemp = OBJpareto(:,:,end);
            if OBJtemp(1) ~= OBJtemp(2)
                for i = 1:nIter
                    OBJi = zeroTruncate(OBJpareto(:,:,i));
                    if i < nIter
                        plot(OBJi(1,:),OBJi(2,:),'*','markersize',6,'color',[0.5 0.5 0.5]);
                        hold on
                    else
                        plot(OBJi(1,:),OBJi(2,:),'b^','markersize',6,'markerfacecolor','b');
                    end
                end
                hold off
                grid
                xlabel('Objective 1')
                ylabel('Objective 2')
            else
                for i = 1:nIter
                    OBJi = OBJpareto(:,:,i);
                    OBJ(i) = OBJi(1);
                end
                plot(OBJ,'-*','markersize',6);
                grid on
                xlabel('Iteration')
                ylabel('LCOE (€/MWh)')
            end
        else
            msgbox('Not support 3 objectives','Warning')
            disp('Not support 3 objectives')
        end
        
    %--------------------------------------------
    % Variable evolution
    %--------------------------------------------
    case 4
        OBJtemp = OBJpareto(:,:,end);
        if nOBJ >= 3
            msgbox('Not support 3 objectives','Warning')
            disp('Not support 3 objectives')
        else
            prompt = {'Choose variable order'};
            dlg_title = '';
            num_lines = 1;
            defaultans = {''};
            objectPos = inputdlg(prompt,dlg_title,num_lines,defaultans);
            
            if ~isempty(objectPos)
                objectPos = str2num(objectPos{1});
                if OBJtemp(1) == OBJtemp(2) && size(OBJpareto,2) == 1
                    CONVparetoMono(1,'var',objectPos,OBJpareto,VARpareto,MISCpareto,saveMatValue);
                else                                                        % This will plot variable along the pareto front only
                    CONVparetoMulti('var',objectPos,lastParetoObj,lastParetoVar,lastParetoMisc,saveMatValue)
                end
            else
                errordlg('No variable chosen','Error')
            end
        end
        
    %--------------------------------------------
    % Misc. evolution
    %--------------------------------------------
    case 5
        OBJtemp = OBJpareto(:,:,end);
        if nOBJ >= 3
            msgbox('Not support 3 objectives','Warning')
            disp('Not support 3 objectives')
        else
            prompt = {'Choose misc. order'};
            dlg_title = '';
            num_lines = 1;
            defaultans = {''};
            objectPos = inputdlg(prompt,dlg_title,num_lines,defaultans);
            
            if ~isempty(objectPos)
                objectPos = str2num(objectPos{1});
                if OBJtemp(1) == OBJtemp(2) && size(OBJpareto,2) == 1
                    CONVparetoMono(1,'misc',objectPos,OBJpareto,VARpareto,MISCpareto,saveMatValue);
                else                                                        % This will plot variable along the pareto front only
                    CONVparetoMulti('misc',objectPos,lastParetoObj,lastParetoVar,lastParetoMisc,saveMatValue)
                end
            else
                errordlg('No misc. chosen','Error')
            end
        end
        
    %--------------------------------------------
    % Boundary touching check
    %--------------------------------------------
    case 6
        [touchVarText,touchBoundText,touchRepText] = boundaryTouchingCheck(multioptOption,lastParetoVar);
        line1 = 'Variables that touch the boundaries:';
        line2 = touchVarText;
        line3 = 'Upper (1) or lower (0) touching:';
        line4 = touchBoundText;
        line5 = 'Touching repetitions:';
        line6 = touchRepText;
        uiwait(msgbox({line1,'', line2,'',line3,'',line4,'',line5,'',line6,'on'},'Boundary touching check'))
end

% % --- Executes during object creation, after setting all properties.
% function axes2_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to axes2 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
% 
% % Hint: place code in OpeningFcn to populate axes2


% --- Executes on button press in saveMat.
function saveMat_Callback(hObject, eventdata, handles)
% hObject    handle to saveMat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of saveMat



% --- Executes on button press in figRotate.
function figRotate_Callback(hObject, eventdata, handles)
% hObject    handle to figRotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of figRotate
if get(hObject,'Value')
    rotate3d on
else
    rotate3d off
end


% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes3
