% Function to determine how many variable touch the upper and lower
% boundaries by checking the last result
function [touchVarText,touchBoundText,touchRepText] = boundaryTouchingCheck(Option,VARsort)

domain = Option.domain;
touchCheck = 0;
tolerance = 0.01;                                                           % 1% from the boundary is said to be on the boundary
for nVar = 1:size(domain,1)
    nTouchUp = 0;
    nTouchLow = 0;
    for nPoint = 1:size(VARsort,2)
        if abs(VARsort(nVar,nPoint) - domain(nVar,2)) < tolerance*abs(domain(nVar,2))
            nTouchUp = nTouchUp + 1;
        end
        if abs(VARsort(nVar,nPoint) - domain(nVar,1)) < tolerance*abs(domain(nVar,1))
            nTouchLow = nTouchLow + 1;
        end
    end
    if nTouchUp >=1 || nTouchLow >= 1
        touchCheck = touchCheck + 1;
        touchVar(touchCheck) = nVar;
        touchPoint(touchCheck) = nTouchUp + nTouchLow;
        touchPos(touchCheck) = nTouchUp/(nTouchUp + nTouchLow);
    end
end

touchVarText = '';
touchRepText = '';
touchBoundText = '';

if touchCheck > 0
    for i = 1:touchCheck
        tempVarText = num2str(touchVar(i));
        tempPointText = num2str(touchPoint(i));
        tempPosText = num2str(touchPos(i));
        touchVarText = [touchVarText,', ',tempVarText(1:min(3,length(tempVarText)))];
        touchRepText = [touchRepText,', ',tempPointText(1:min(3,length(tempPointText)))];
        touchBoundText = [touchBoundText,', ',tempPosText(1:min(3,length(tempPosText)))];
    end
    touchVarText(1) = '';
    touchRepText(1) = '';
    touchBoundText(1) = '';
else
    touchVarText = 'Ok';
    touchRepText = 'Ok';
    touchBoundText = 'Ok';
end

if isfield(Option,'TouchHandle')
    set(Option.TouchHandle.TouchVar,'string',touchVarText);
    set(Option.TouchHandle.TouchPoint,'string',touchRepText);
    set(Option.TouchHandle.TouchPos,'string',touchBoundText);
    
    % Set warning color, white if no touching, yellow if less than 3 and
    % red if more than 4
    whiteCode = [1 1 1];
    yellowCode = [1 1 0];
    redCode = [1 0 0];
    if touchCheck == 0
        colorCode = whiteCode;
    elseif touchCheck <= 4
        colorCode = yellowCode;
    else
        colorCode = redCode;
    end
    
    set(Option.TouchHandle.TouchVar,'BackgroundColor',colorCode);
    set(Option.TouchHandle.TouchPoint,'BackgroundColor',colorCode);
    set(Option.TouchHandle.TouchPos,'BackgroundColor',colorCode);
    
end