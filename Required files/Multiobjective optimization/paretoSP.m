% Function to calculate diversity of the pareto front in bi-objective
% optimization
% It is calculated as standard deviation of based on euclidean distance
% from one points to other
% Input: 
%   y: vector point in the pareto front
% Output: 
%   SP: diversity index
% Smaller SP means a better diversity
function SP = paretoSP()
% load Result_MOPSO
load Result_NSGA2
y = OBJpareto(:,:,end);
y = zeroTruncate(y);                                                        % Cut all trailing zeros
nPoint = size(y,2);                                                         % Number of points on the pareto front
for i = 1:nPoint
    temp = sum(abs(repmat(y(:,i),1,nPoint) - y),1);
    temp(:,i) = [];                                                         % Remove the ``ith'' column which is zeros resulting by point i - point i
    d(i) = min(temp);    
end

% Taking standard deviation
SP = std(d);
