% Function to run objective space and verification againts constraints from
% a preset design space

function [Objective,OBJviolate,OBJpareto,VARpareto] = presetRun(Option)

% Objective function parameters
Fcn = Option.Fcn;
domain = Option.domain;

% Saving parameter
saveFile = 'Result_preset';                                                 % Result file name

if exist([saveFile,'.mat'],'file')
    delete([saveFile,'.mat'])                                               % Delete result file if existed
end

% First trial generation
uiopen('load')
Variable = preset;                                                          % Simply all the variable needed to be run
% Effet quantique pour les variables discretes
Variable = forceDomain(Variable,domain);
% Evaluation of trials
[Objective,Constraint,Misc] = feval(Fcn,Variable);
Somme = sum(max(Constraint,0),1);
indexPos = find(Somme > 0);
OBJviolate = Objective(:,indexPos);
OBJpareto = Objective;
CONSTRpareto = Constraint;
MISCpareto = Misc;
OBJpareto(:,indexPos) = [];
CONSTRpareto(:,indexPos) = [];
MISCpareto(:,indexPos) = [];
VARpareto = Variable;
VARpareto(:,indexPos) = [];
if ~isempty(OBJpareto)
    [~,~,MISCpareto] = paretoFront(OBJpareto,0,VARpareto,MISCpareto);
    [OBJpareto,VARpareto,CONSTRpareto] = paretoFront(OBJpareto,0,VARpareto,CONSTRpareto);
    save(saveFile,'OBJpareto','VARpareto','CONSTRpareto','MISCpareto')
    if Option.plotDisplay
        objectiveDisplay(Objective,OBJviolate,OBJpareto)
    end
    
else
    OBJpareto = Objective;
    VARpareto = Variable;
    CONSTRpareto = Constraint;
    MISCpareto = Misc;
    NotePareto = 'All points are unfeasible';
    save(saveFile,'OBJpareto','VARpareto','CONSTRpareto','MISCpareto','NotePareto')
    if Option.plotDisplay
        objectiveDisplay(Objective,OBJviolate,[])
    end
    display('No feasible solution found')
end
if Option.exportToExcel
    exportToExcel(OBJpareto,VARpareto,CONSTRpareto,MISCpareto,saveFile);
end

