%% - FILE DATA_WF.MAT MUST HAVE BEEN CREATED BEFORE -

%% Function
function varargout = multiopt(varargin)
% multiopt MATLAB code for multiopt.fig
%      multiopt, by itself, creates a new multiopt or raises the existing
%      singleton*.
%
%      H = multiopt returns the handle to a new multiopt or the handle to
%      the existing singleton*.
%
%      multiopt('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in multiopt.M with the given input arguments.
%
%      multiopt('Property','Value',...) creates a new multiopt or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before multiopt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to multiopt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help multiopt

% Last Modified by GUIDE v2.5 14-Jun-2017 22:24:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @multiopt_OpeningFcn, ...
    'gui_OutputFcn',  @multiopt_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before multiopt is made visible.
function multiopt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to multiopt (see VARARGIN)

% Choose default command line output for multiopt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
try
    imshow(imread('optStart.jpg'));
end

% UIWAIT makes multiopt wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = multiopt_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in optAlgorithm.
function optAlgorithm = optAlgorithm_Callback(hObject, eventdata, handles)
% hObject    handle to optAlgorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
optCheck = get(hObject,'Value');
geyCode = 0.863*ones(1,3);
switch optCheck
    case 1
        set(handles.changeParam,'enable','on');
        set(handles.nIterationText,'Visible','on')
        set(handles.nIteration,'Visible','on')
        set(handles.reload,'Visible','on')
        set(handles.nIndividualText,'String','No of particles')
        set(handles.Pannel1,'Visible','on')
        set(handles.nIndividualText,'Visible','on')
        set(handles.nIndividual,'Visible','on')
        set(handles.samplingMethod,'Visible','on')
        set(handles.TouchVar,'string','') 
        set(handles.TouchPoint,'string','')
        set(handles.TouchPos,'string','')
        set(handles.TouchVar,'BackgroundColor',[1 1 1]);
        set(handles.TouchPoint,'BackgroundColor',[1 1 1]);
        set(handles.TouchPos,'BackgroundColor',[1 1 1]);
    case 2
        set(handles.changeParam,'enable','on');
        set(handles.nIterationText,'Visible','on')
        set(handles.nIteration,'Visible','on')
        set(handles.reload,'Visible','on')
        set(handles.nIndividualText,'String','No of individuals')
        set(handles.Pannel1,'Visible','on')
        set(handles.nIndividualText,'Visible','on')
        set(handles.nIndividual,'Visible','on')
        set(handles.samplingMethod,'Visible','on')
        set(handles.TouchVar,'string','') 
        set(handles.TouchPoint,'string','')
        set(handles.TouchPos,'string','')
        set(handles.TouchVar,'BackgroundColor',[1 1 1]);
        set(handles.TouchPoint,'BackgroundColor',[1 1 1]);
        set(handles.TouchPos,'BackgroundColor',[1 1 1]);
    case 3
        set(handles.changeParam,'enable','off');
        set(handles.nIterationText,'Visible','off')
        set(handles.nIteration,'Visible','off')
        set(handles.reload,'Visible','off')
        set(handles.nIndividualText,'String','No of points')                 % Number of points to do screening, including the upper and lower bounds
        set(handles.Pannel1,'Visible','on')
        set(handles.nIndividualText,'Visible','on')
        set(handles.nIndividual,'Visible','on')
        set(handles.samplingMethod,'Visible','off')
        set(handles.TouchVar,'string','N/A') 
        set(handles.TouchPoint,'string','N/A')
        set(handles.TouchPos,'string','N/A')
        set(handles.TouchVar,'BackgroundColor',geyCode);
        set(handles.TouchPoint,'BackgroundColor',geyCode);
        set(handles.TouchPos,'BackgroundColor',geyCode);
    case 4
        set(handles.changeParam,'enable','off');
        set(handles.nIterationText,'Visible','off')
        set(handles.nIteration,'Visible','off')
        set(handles.reload,'Visible','off')
        set(handles.nIndividualText,'String','No of runs')
        set(handles.Pannel1,'Visible','on')
        set(handles.nIndividualText,'Visible','on')
        set(handles.nIndividual,'Visible','on')
        set(handles.samplingMethod,'Visible','on')
        set(handles.TouchVar,'string','N/A') 
        set(handles.TouchPoint,'string','N/A')
        set(handles.TouchPos,'string','N/A')
        set(handles.TouchVar,'BackgroundColor',geyCode);
        set(handles.TouchPoint,'BackgroundColor',geyCode);
        set(handles.TouchPos,'BackgroundColor',geyCode);
    case 5
        set(handles.changeParam,'enable','off');
        set(handles.nIterationText,'Visible','off')
        set(handles.nIteration,'Visible','off')
        set(handles.reload,'Visible','off')
        set(handles.nIndividualText,'Visible','off')
        set(handles.nIndividual,'Visible','off')
        set(handles.samplingMethod,'Visible','off')
        set(handles.TouchVar,'string','N/A') 
        set(handles.TouchPoint,'string','N/A')
        set(handles.TouchPos,'string','N/A')
        set(handles.TouchVar,'BackgroundColor',geyCode);
        set(handles.TouchPoint,'BackgroundColor',geyCode);
        set(handles.TouchPos,'BackgroundColor',geyCode);
end


% Hints: contents = cellstr(get(hObject,'String')) returns optAlgorithm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from optAlgorithm

% --- Executes during object creation, after setting all properties.
function optAlgorithm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to optAlgorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in reload.
function reload_Callback(hObject, eventdata, handles)
% hObject    handle to reload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of reload


% --- Executes on button press in plotDisplay.
function plotDisplay_Callback(hObject, eventdata, handles)
% hObject    handle to plotDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotDisplay



function nIndividual_Callback(hObject, eventdata, handles)
% hObject    handle to nIndividual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nIndividual as text
%        str2double(get(hObject,'String')) returns contents of nIndividual as a double


% --- Executes during object creation, after setting all properties.
function nIndividual_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nIndividual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in optRun.
function optRun_Callback(hObject, eventdata, handles)
% hObject    handle to optRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Setting parallel computing
% Check if the input file are available (myFun and designDomain)
clc
% multioptCodeCheck(2019);    
set(handles.TouchVar,'BackgroundColor',[1 1 1]);
set(handles.TouchPoint,'BackgroundColor',[1 1 1]);
set(handles.TouchPos,'BackgroundColor',[1 1 1]);

if exist('myFun.m','file') && exist('designDomain.m','file')
    % Parallel computing options
    nCluster = str2num(get(handles.NoCluster,'String'));
    if nCluster >=2 && floor(nCluster) == nCluster
        parPoolRun(nCluster);
    elseif nCluster == 1
        delete(gcp('nocreate'));
    else
        error('Wrong number of clusters')
    end
    
    % Waiting the first solution screen
    
    try
        imshow(imread('optWaiting.jpg'));
    end
    
    % Figure handling
    set(handles.figRotate,'Visible','off')
    
    % Get input from edit text box
    nIndividual = str2num(get(handles.nIndividual,'String'));
    nIteration = str2num(get(handles.nIteration,'String'));
    paretoCheck = [0 9];
    
    
    % Algorithm choosing (MOPSO, NSGA2 or random run)
    optAlgorithm = get(handles.optAlgorithm,'Value');
    
    [domain, UserInput] = designDomain();                                   % Design variables
    TouchHandle.TouchVar = handles.TouchVar;
    TouchHandle.TouchPoint = handles.TouchPoint;
    TouchHandle.TouchPos = handles.TouchPos;
    
    
    Option = struct(...
        'TouchHandle',      TouchHandle,...                                 % GUI handles
        'nIndividual',      nIndividual,...                                 % No of particles for MOPSO, individual for NSGA-II, points for screening, and runs for random run.
        'nIteration',       nIteration,...                                  % No of iterations for MOPSO and generation for NSGA-II
        'maxTrial',         10,...                                          % Maximum no of trials to find at least 1 feasible solution
        'samplingMethod',   [],...                                          % Sampling method
        'paretoCheck',      paretoCheck,...                                 % Check pareto quality, stop if can not improve pareto front
        'Fcn',              @(Option) myFun(Option,UserInput),...          	% Objectives and constraints functions
        'reload',           get(handles.reload,'Value'),...                	% Option to reload from an interrupted optimization
        'plotDisplay',      get(handles.plotDisplay,'Value'),...           	% True: Plot result
        'exportToExcel',    get(handles.exportToExcel,'Value'));           	% Export pareto front to excel
    
    Option = var2struct(Option,domain);                                     % Add domain field to the Option
    
    % Default parameter setting for MOPSO and NSGA-II
    Option.StraParam = struct(...
        ...     % For MOPSO
        'accel_memory',     1,...                                           % Cognitive Acceleration
        'accel_guide',      1,...                                           % Sociale Acceleration
        'inertia_start',    0.8,...                                         % Inertial value at the begining
        'inertie_end',      0.8,...                                         % Inertial value at the end
        'proba_mut',        0.01,...                                        % Proportion of particules muted
        'fact_constrict',   0.5,...                                         % Constriction factor
        ...     % For NSGA-II
        'pX',               1,...                                           % Crossover probability
        'etaX',             1,...                                           % Distribution parameter for simulated binary crossover 
        'pM',               0.1,...                                         % Mutation probability
        'etaM',             0.1);                                           % Distribution parameter for polynomial mutation
    
    if isfield(handles.mopsoParam,'accel_memory')
        Option.StraParam.accel_memory = handles.mopsoParam.accel_memory;      % Cognitive Acceleration
        Option.StraParam.accel_guide = handles.mopsoParam.accel_guide;        % Sociale Acceleration
        Option.StraParam.inertia_start = handles.mopsoParam.inertia_start;    % Inertial value at the begining
        Option.StraParam.inertie_end = handles.mopsoParam.inertie_end;        % Inertial value at the end
        Option.StraParam.proba_mut = handles.mopsoParam.proba_mut;            % Proportion of particules muted
        Option.StraParam.fact_constrict = handles.mopsoParam.fact_constrict;  % Constriction factor
    end
    
    if isfield(handles.nsga2Param,'pX')
        Option.StraParam.pX = handles.nsga2Param.pX;
        Option.StraParam.etaX = handles.nsga2Param.etaX;
        Option.StraParam.pM = handles.nsga2Param.pM;
        Option.StraParam.etaM = handles.nsga2Param.etaM;
    end
    
    switch get(handles.samplingMethod,'Value')
        case 1
            Option.samplingMethod = 'LHS';                                  % Latin Hypercube Sampling
        case 2
            Option.samplingMethod = 'SUD';                                  % Standard Uniform Distribution
    end
    
    set(hObject,'String','Running')
    set(handles.optStateInfo,'String','Waiting is happiness')               % Message when being under running process
    drawnow;
    
    tic
    geyCode = 0.863*ones(1,3);
    switch optAlgorithm
        case 1
            set(handles.TouchVar,'String','');
            set(handles.TouchPoint,'String','');
            set(handles.TouchPos,'String','');
            drawnow;
            MOPSO(Option);
            resultFile = 'Result_MOPSO.mat';
        case 2
            set(handles.TouchVar,'String','');
            set(handles.TouchPoint,'String','');
            set(handles.TouchPos,'String','');
            drawnow;
            NSGA2(Option);
            resultFile = 'Result_NSGA2.mat';
        case 3
            set(handles.TouchVar,'BackgroundColor',geyCode);
            set(handles.TouchPoint,'BackgroundColor',geyCode);
            set(handles.TouchPos,'BackgroundColor',geyCode);
            screenRun(Option);
        case 4
            set(handles.TouchVar,'BackgroundColor',geyCode);
            set(handles.TouchPoint,'BackgroundColor',geyCode);
            set(handles.TouchPos,'BackgroundColor',geyCode);
            randomRun(Option);
        case 5
            set(handles.TouchVar,'BackgroundColor',geyCode);
            set(handles.TouchPoint,'BackgroundColor',geyCode);
            set(handles.TouchPos,'BackgroundColor',geyCode);
            presetRun(Option);
    end
    toc
    elapseTime = ceil(toc/60*1e3)/1e3;                                      % Computation time in minutes
    set(hObject,'String','Start')
    set(handles.optStateInfo,'String',['Bingo !  ',num2str(elapseTime),' mins'])       	% Message when being under running process
    
    % Showing conlusion figure
    if exist('resultFile','var')
        if exist(resultFile,'file')
            load(resultFile,'feasibleConclusion')
            if feasibleConclusion
                % Setting rotation on if 3D plot presented
                [a,~] = view;
                if a ~= 0
                    set(handles.figRotate,'Visible','on')
                end
            else
                try
                    imshow(imread('optNoSolution.jpg'));
                catch
                    disp('No solution found')
                end
            end
        else
            try
                imshow(imread('optNoSolution.jpg'));
            catch
                disp('No solution found')
            end
        end
    else
        [a,~] = view;
        if a ~= 0
            set(handles.figRotate,'Visible','on')
        end
    end
else
    uiwait(errordlg('No input function and design domain found','Error'));
    return
end

function nIteration_Callback(hObject, eventdata, handles)
% hObject    handle to nIteration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nIteration as text
%        str2double(get(hObject,'String')) returns contents of nIteration as a double


% --- Executes during object creation, after setting all properties.
function nIteration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nIteration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxTrial_Callback(hObject, eventdata, handles)
% hObject    handle to maxTrial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxTrial as text
%        str2double(get(hObject,'String')) returns contents of maxTrial as a double


% --- Executes during object creation, after setting all properties.
function maxTrial_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxTrial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function paretoCheck_Callback(hObject, eventdata, handles)
% hObject    handle to paretoCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of paretoCheck as text
%        str2double(get(hObject,'String')) returns contents of paretoCheck as a double


% --- Executes during object creation, after setting all properties.
function paretoCheck_CreateFcn(hObject, eventdata, handles)
% hObject    handle to paretoCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function nConsecutive_Callback(hObject, eventdata, handles)
% hObject    handle to nConsecutive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nConsecutive as text
%        str2double(get(hObject,'String')) returns contents of nConsecutive as a double


% --- Executes during object creation, after setting all properties.
function nConsecutive_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nConsecutive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in exportToExcel.
function exportToExcel_Callback(hObject, eventdata, handles)
% hObject    handle to exportToExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of exportToExcel


% --- Executes during object creation, after setting all properties.

% hObject    handle to paretoPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate paretoPlot


% --- Executes on button press in parPool.
function parPool_Callback(hObject, eventdata, handles)
% hObject    handle to parPool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of parPool


% --- Executes on button press in figRotate.
function figRotate_Callback(hObject, eventdata, handles)
% hObject    handle to figRotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of figRotate
if get(hObject,'Value')
    rotate3d on
else
    rotate3d off
end


% --- Executes on selection change in samplingMethod.
function samplingMethod_Callback(hObject, eventdata, handles)
% hObject    handle to samplingMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns samplingMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from samplingMethod


% --- Executes during object creation, after setting all properties.
function samplingMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to samplingMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function about_Callback(hObject, eventdata, handles)
% hObject    handle to about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
multioptAbout;


% --------------------------------------------------------------------
function howToRun_Callback(hObject, eventdata, handles)
% hObject    handle to howToRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
line1 = 'The program should work without any problem with Matlab 2015b or later';
line2 = 'Do the following to run the program:';
line3 = '1. Open a folder containing designDomain.m and myFun.m function files while this GUI being opened.';
line4 = '2. Fill and choose appropriate options in the GUI.';
line5 = '3. Enjoy your favourite coffee while waiting.';
uiwait(msgbox({line1,'', line2,'',line3,'',line4,'',line5},'How to run'))


function NoCluster_Callback(hObject, eventdata, handles)
% hObject    handle to NoCluster (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoCluster as text
%        str2double(get(hObject,'String')) returns contents of NoCluster as a double


% --- Executes during object creation, after setting all properties.
function NoCluster_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoCluster (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function upperTouch_Callback(hObject, eventdata, handles)
% hObject    handle to TouchVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TouchVar as text
%        str2double(get(hObject,'String')) returns contents of TouchVar as a double


% --- Executes during object creation, after setting all properties.
function TouchVar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TouchVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function lowerTouch_Callback(hObject, eventdata, handles)
% hObject    handle to lowerTouch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lowerTouch as text
%        str2double(get(hObject,'String')) returns contents of lowerTouch as a double


% --- Executes during object creation, after setting all properties.
function lowerTouch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowerTouch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function checkResult_Callback(hObject, eventdata, handles)
% hObject    handle to checkResult (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
multioptCheck;


% --------------------------------------------------------------------
function tool_Callback(hObject, eventdata, handles)
% hObject    handle to tool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function changeParam_Callback(hObject, eventdata, handles)
% hObject    handle to changeParam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
optAlgorithm = get(handles.optAlgorithm,'Value');
num_lines = [1,45];

switch optAlgorithm
    case 1
        prompt = {'Acceleration memory                                ','Acceleration guide','Start inertia','End inertia',...
            'Mutation probability','Constriction factor'};
        dlg_title = 'MOPSO setting';
        
        defaultans = {'1','1','0.8','0.8','0.01','0.5'};
        mopsoParam = inputdlg(prompt,dlg_title,num_lines,defaultans,'on');
        if ~isempty(mopsoParam)
            handles.mopsoParam = struct(...
                'accel_memory',     str2num(mopsoParam{1}),...
                'accel_guide',      str2num(mopsoParam{2}),...
                'inertia_start',    str2num(mopsoParam{3}),...
                'inertie_end',      str2num(mopsoParam{4}),...
                'proba_mut',        str2num(mopsoParam{5}),...
                'fact_constrict',   str2num(mopsoParam{6}));
        end
    case 2
        prompt = {'Crossover probability: pX','Distribution parameter: etaX',...
            'Mutation probability: pM','Distribution parameter: etaM'};
        dlg_title = 'NSGA-II setting';
        defaultans = {'1','1','0.1','0.1'};
        nsga2Param = inputdlg(prompt,dlg_title,num_lines,defaultans,'on');
        if ~isempty(nsga2Param)
            handles.nsga2Param = struct(...
                'pX',               str2num(nsga2Param{1}),...
                'etaX',             str2num(nsga2Param{2}),...
                'pM',               str2num(nsga2Param{3}),...
                'etaM',             str2num(nsga2Param{4}));
        end
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function mopsoParam_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mopsoParam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
