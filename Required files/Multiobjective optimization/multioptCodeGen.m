% Function to generate the activation code for the multiopt program
function y = multioptCodeGen(VSN)

y = round(sum(VSN.^1.6 + sqrt(VSN) + 1985*ones(1,length(VSN))));