%forcer_domaines : Permet de conserver les particules � l'interieur de
%l'espace de Recherche
%[Essaim,Vitesses] = forcer_domaine(Essaim,Vitesses,Domaine)
%
%ARGUMENTS
%   Essaim
%       Vecteur de taille N_variables*N_particules
%       contient les param�tres de N_particules (population)
%       Vecteur de taille N_objectifs*N_particules
%       contient les valeurs des objectifs de Essaim
%   Domaine
%       Vecteur de taille N_variables*2
%       d�finissant les valeurs min et max de chaque param�tres
%       d'optimisation
%
%DONNEES DE RETOUR
%
%   Essaim
%       Vecteur r�actualis�

function [Essaim] = forceDomain_v2(Essaim,Domaine, Vitesses)

N_particules = length(Essaim(1,:));
%Souvenir: Domaine = repmat([0 1 1],n*m,1); %nxm variables of optimization

if length(Domaine(1,:)==4)
    for var=1:length(Domaine(:,3))
        if Domaine(var,3)~=0 %not continuous
            if (Domaine(var,1)==0) &&(Domaine(var,1)==0) %is binary
                SigmoidV = 2*abs((1./(1 + exp(-Vitesses))) - 0.5);
                for part=1:length(Essaim(1,:))
                    if (SigmoidV(var,part)>rand())
                        Essaim(var,part)= 1 - Essaim(var,part);
                    end
                end
            else %discrete but not binary
                Essaim(var,:) = Essaim(var,:) + Vitesses(var,:);
                Ecart=mod(Essaim(var,:),Domaine(var,3)); %division reminder. Domaine(i,3)=1
                Essaim(var,:)=Essaim(var,:)-Ecart+Domaine(var,3)*ones(1,N_particules).*(Ecart/Domaine(var,3)>rand(1,N_particules));
            end
        else %continuous
            Essaim(var,:) = Essaim(var,:) + Vitesses(var,:);
        end
    end
end

Essaim = max(repmat(Domaine(:,1),1,N_particules),Essaim);
Essaim = min(repmat(Domaine(:,2),1,N_particules),Essaim);