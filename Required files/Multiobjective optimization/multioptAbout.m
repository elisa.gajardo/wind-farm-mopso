line11 = 'Multi-objective Optimization featured with MOPSO (developed by Judicael - SATIE) and NSGA-II ver. 4.0 (developed by Kalyanmoy Deb et al.).';
line12 = ' Both are modified, combined, and added some features for the convenient utilization and quality improvement as well. Some main added';
line13 = ' features are as follows:';
line2 = '1. Screeening: is to check sensitivities of variables.';
line3 = '2. Random: is to run with random variable sets.';
line4 = '3. Preset: run from a predefined variable set.';
line5 = '4. Latin Hypercube Sampling technique.';
line6 = '5. Easy to check result: export to excel, etc.';
line7 = '6. Boundary checking: is to check if variables stay on the boundaries during the optimization.';
line8 = '           Ver. 6.1 - 06/2017 by Hoang Trung - Kien: kienht26@gmail.com';
uiwait(msgbox({[line11 line12 line13],'',line2,'',line3,'',line4,'',line5,'',line6,'',line7,'',line8},'About'))