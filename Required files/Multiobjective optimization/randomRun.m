% Function to randomly run objective space and verification againts constraints

function [Objective,OBJviolate,OBJpareto,VARpareto,feasibleSolution] = randomRun(Option)
if ~isfield(Option,'save')
    Option.save = 1;
end
    
% Initialisation du generateur aleatoire
rand('state',sum(100*clock));

N_runs = Option.nIndividual;

set(0,'RecursionLimit',100)

% Objective function parameters
fonction = Option.Fcn;
domain = Option.domain;

% Saving parameter
saveFile = 'Result_random';                                                 % Result file name

if exist([saveFile,'.mat'],'file')
    delete([saveFile,'.mat'])                                               % Delete result file if existed
end

% First trial generation
Variable = x0Gen(domain,N_runs,Option.samplingMethod);
% Effet quantique pour les variables discretes
Variable = forceDomain(Variable,domain);
% Evaluation of trials
[Objective,Constraint,Misc] = feval(fonction,Variable);
Somme = sum(max(Constraint,0),1);
indexPos = find(Somme > 0);
OBJviolate = Objective(:,indexPos);
OBJpareto = Objective;
VARpareto = Variable;
CONSTRpareto = Constraint;
MISCpareto = Misc;

CONSTRpareto(:,indexPos) = [];
OBJpareto(:,indexPos) = [];
VARpareto(:,indexPos) = [];
MISCpareto(:,indexPos) = [];

if ~isempty(OBJpareto)
    [~,~,MISCpareto] = paretoFront(OBJpareto,0,VARpareto,MISCpareto);
    [OBJpareto,VARpareto,CONSTRpareto] = paretoFront(OBJpareto,0,VARpareto,CONSTRpareto);
    if Option.save
        save(saveFile,'OBJpareto','VARpareto','CONSTRpareto','MISCpareto','Objective','Variable','Constraint','Misc')
    end
    if Option.plotDisplay
        objectiveDisplay(Objective,OBJviolate,OBJpareto);
    end
    feasibleSolution = 1;
    
else
    OBJpareto = Objective;
    VARpareto = Variable;
    CONSTRpareto = Constraint;
    MISCpareto = Misc;
    NotePareto = 'All points are unfeasible';
    feasibleSolution = 0;
    if Option.save
        save(saveFile,'OBJpareto','VARpareto','CONSTRpareto','MISCpareto','NotePareto')
    end
    if Option.plotDisplay
        objectiveDisplay(Objective,OBJviolate,[]);
        display('No feasible solution found')
    end
    
end
if Option.exportToExcel
    exportToExcel(OBJpareto,VARpareto,CONSTRpareto,MISCpareto,saveFile);
end
