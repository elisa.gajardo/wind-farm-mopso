function multioptCodeCheck(expYear)
%------------------------------------------------------------
% Get volID
%------------------------------------------------------------
currentYear = getCurrentTime();                                             % Current year
if currentYear >= expYear
    currentFolder = pwd;                                                    % Current folder
    rootFld = matlabroot;
    passDrive = rootFld(1);                                                 % Drive name of the matlab root folder
    passFolder = [passDrive,':\MultiOpt\Bin\'];                             % Folder to store password
    if ~exist(passFolder,'dir')
        mkdir(passFolder);
    end
    cd(passFolder);

    [~, out] = dos('vol');
    sc = strsplit(out,'\n');
    VSN = sc{2}(end-3:end);
    
    %------------------------------------------------------------
    % Alter the VSN
    %------------------------------------------------------------
    newVSN = codeVSN(VSN);
    
    %------------------------------------------------------------
    % % Alter the volID
    %------------------------------------------------------------
    activateCode =  multioptCodeGen(newVSN);                                 % Generating activation code
    
    %------------------------------------------------------------
    % Check activation
    %------------------------------------------------------------
    passFile = 'MultiOptPass';
    activateCheck = 0;
    if exist([passFile,'.mat'],'file') == 2
        load(passFile)
        if strcmpi(inputPass,num2str(activateCode));
            activateCheck = 1;
        end
    end
    
    %------------------------------------------------------------
    if activateCheck == 0
        prompt = {['Enter an activation code or contact kienht26@gmail.com with your serial number ''',num2str(newVSN),'''']};
        dlg_title = 'Activation guide';
        num_lines = [1,60];
        defaultans = {''};
        inputPass = inputdlg(prompt,dlg_title,num_lines,defaultans);
        if isempty(inputPass)
            inputPass = {'00'};
        end
        inputPass = inputPass{1};
        if strcmpi(inputPass,num2str(activateCode))
            %------------------------------------------------------------
            % Create the permanent activation file
            %------------------------------------------------------------
            save(passFile,'inputPass')
            uiwait(msgbox('Program activated ... ','Done'));
        else
            cd(currentFolder);
            multioptAbout;
            uiwait(errordlg('Wrong activation code ! ','Error'));
            error('Wrong activation code')
        end
    end
    cd(currentFolder);
end