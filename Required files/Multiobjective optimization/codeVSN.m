% Function to generate the activation code for the multiopt program
function y = codeVSN(VSN)
doubleVSN = double(VSN);
y = doubleVSN(1)^2 - doubleVSN(2) + doubleVSN(3)^3 - doubleVSN(4);