% UserInput = 6; optAlgorithm = 'MOPSO';


function MultiOpt_run(UserInput,optAlgorithm)

[domain, UserInput] = designDomain(UserInput);                              % Design domain space

Option = struct(... 
    'nIndividual',          100,...                                         % No of particles for MOPSO and individual for NSGA-II
    'nIteration',           30,...                                          % No of iterations for MOPSO and generation for NSGA-II
    'maxTrial',             16,...                                           % Maximum no of trials to find at least 1 feasible solution
    'samplingMethod',      	'LHS',...                                       % Sampling technique for initial start (LHS or standard uniform distribution)
    'paretoCheck',          [0.0 6],...                                     % Check pareto quality, stop if can not improve pareto front
    'Fcn',                  @(variable) myFun(variable,UserInput),...      	% Objectives and constraints functions
    'reload',               false,...                                       % Option to reload from an interrupted optimization 
   	'plotDisplay',          true,...                                       	% True: Plot result
    'exportToExcel',        false...                                        % Export pareto front (variables and objective) to excel file
    );

Option = var2struct(Option,domain);                                         % Add domain field to the Option

switch upper(optAlgorithm)
    case 'MOPSO'
        MOPSO(Option);
    case 'NSGA2'
        NSGA2(Option);
    case 'RANDOMRUN'
        randomRun(Option);
    otherwise
        error('Invalid algorithm for optimization');
end
