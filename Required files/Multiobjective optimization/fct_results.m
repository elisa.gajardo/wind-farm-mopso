function [] = fct_results()
%FCT_RESULTS Summary of this function goes here
%   Detailed explanation goes here
    data_result = load('Result_MOPSO.mat');
    optimal_Particle = data_result.lastParetoVar(:,1);
    optimal_LCOE = data_result.lastParetoObj(:,1);
    N_wt = length(find(optimal_Particle==1))
    [~,is_wt_grid, wind_occurence, ~,L,x_wt_index, y_wt_index, boundaries, date_UTC, coord_onshore_subst,~, Pnom] = fct_load_data();
    
    LCOE =optimal_LCOE(1);
    optimal_WF = fct_fill_matrix(is_wt_grid,optimal_Particle)
    %[Energy, Costs] = fct_energy_costs(is_wt_grid, wind_occurence, x_wt_index,y_wt_index,date_UTC, L,coord_onshore_subst,Pnom)
    plot_wf(x_wt_index,y_wt_index,optimal_WF,boundaries,LCOE)
    
    %[turbines_graph, graph_grid,subst_coord]=fct_cabling_cluster(3,optimal_WF,x_wt_index,y_wt_index,L,Pnom);
    %fct_cabling_plot(turbines_graph, subst_coord, graph_grid, x_wt_index, y_wt_index,L)
    
    
end 

    
    

    

