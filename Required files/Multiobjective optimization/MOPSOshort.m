%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       MOPSO program siplified for sub-optimization    	%
%                  Modified by K. HOANG               	    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Note: Simplified MOPSO without display, saving any result

function [VARpareto, OBJpareto, MISCpareto] = MOPSOshort(Option)

%------------------------------------------
% Strategy parameter setting
%------------------------------------------
Option.StraParam = struct(... 
    'accel_memory',     1,...                                               % Cognitive Acceleration
    'accel_guide',      1,...                                               % Sociale Acceleration
    'inertia_start',    0.8,...                                             % Inertial value at the begining
    'inertie_end',      0.8,...                                             % Inertial value at the end
    'proba_mut',        0.01,...                                            % Proportion of particules muted
    'fact_constrict',   0.5);                                               % Constriction factor

% Initialisation du generateur aleatoire
rand('state',sum(100*clock));

% Algorithm's parameters
N_variables = size(Option.domain,1);
N_particules = Option.nIndividual;
N_iterations = Option.nIteration;
N_archive = 36;                                                             % Present 36 points on the pareto front

set(0,'RecursionLimit',100)

% Objective function parameters
fonction = Option.Fcn;
domain = Option.domain;

% Strategy parameters
Inertie_debut = Option.StraParam.inertia_start;
Inertie_fin = Option.StraParam.inertie_end;
Accel_memoire = Option.StraParam.accel_memory;
Accel_guide = Option.StraParam.accel_guide;
Proba_mut = Option.StraParam.proba_mut;
Fact_constrict = Option.StraParam.fact_constrict;

if ~Option.reload
    % First trial generation (forced to domain included)
    Essaim = x0Gen(domain,N_particules,Option.samplingMethod);
    % Velocity initialization
    Vitesses = repmat(domain(:,1),1,N_particules) + repmat(domain(:,2)-domain(:,1),1,N_particules).*rand(N_variables,N_particules);
    Vitesses(:) = 0;
    % Evaluation of trials
    [Objectifs,Contraintes,Divers] = feval(fonction,Essaim);
    % Initialisation des m�moires individuelles
    Memoires_Variables = Essaim;
    Memoires_Objectifs = Objectifs;
    Memoires_Contraintes = Contraintes;
    [Memoires_Variables,Memoires_Objectifs,Memoires_Contraintes,Somme_Viols_Memoires] ...
        = MAJ_Memoires(Essaim,Objectifs,Contraintes,Memoires_Variables,Memoires_Objectifs,Memoires_Contraintes);
    % Initialiation de l'archive
    if length(Objectifs(:,1))==2
        [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] ...
            = extraire_front(Essaim,Objectifs,Contraintes,Divers);
    elseif length(Objectifs(:,1))==3
        [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] ...
            = extraire_front_3obj(Essaim,Objectifs,Contraintes,Divers);
    else
        disp('error')
        return
    end
    % Ajustement de l'archive
    [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers,Distance] ...
        = ajuster_archive(Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers,N_archive);
    
    Mut=false(1,N_particules);
    Guides=ones(1,N_particules);
    % A executer pendant N_iteration
    cycle = 1;
    cycle_temp = 1;
else
    load BackUp_MOPSO -regexp ^(?!Option$).                                 % Load recovery file except Option variable
    % Reset number of iterations and archive if intended
    N_iterations = Option.nIteration;
    N_archive = 36;
end

% Initialize number of iterations that can not improve 5% in each OBJ dimension
nImprove = 0;
while (cycle <= N_iterations && cycle_temp <= Option.maxTrial && nImprove < Option.paretoCheck(2))
    startTime = datetime('now');
    % Choix d'un guide pour chaque particule
    Guides = choisir_guides(N_particules,Objectifs,Front_Pareto_Objectifs,Somme_Viols_Memoires,Guides,Mut);
    % Determination de la vitesse de chaque particules
    % Vitesse individuelle
    U1 = Accel_memoire*rand(N_variables,N_particules);
    Vitesse_individuelle = U1.*(Memoires_Variables-Essaim);
    % Acceleration sociale
    if isempty(Front_Pareto_Variables)
        U2 = Accel_guide*rand(N_variables,N_particules);
        Vitesse_sociale = U2.*(Memoires_Variables(:,Guides)-Essaim);
    else
        U2 = Accel_guide*rand(N_variables,N_particules);
        Vitesse_sociale = U2.*(Front_Pareto_Variables(:,Guides)-Essaim);
    end
    % Acceleration inertielle
    Vitesse_inertielle=(Inertie_debut+(Inertie_fin-Inertie_debut)*cycle/N_iterations)*Vitesses;
    % Calcul de la vitesse globale
    Vitesses = Vitesse_inertielle+Vitesse_individuelle+Vitesse_sociale;
    %Constriction de la vitesse
    Vitesses = (1-(1-Fact_constrict)*cycle/N_iterations)*Vitesses;
    
    % Rajouter une perturbation
    Mut = false(1,N_particules);
    if cycle <= 2/3*N_iterations
        parfor i = 1:N_particules
            Indice = find(all(repmat(Essaim(:,i),1,size(Front_Pareto_Variables,2))==Front_Pareto_Variables));
            if Indice
                Mut(i) = true;
            end
        end
    else
        Mut = rand(1,N_particules);
        Mut = Mut < (1-(1-Fact_constrict)*cycle/N_iterations)*Proba_mut;
    end
    
    Perturbation = 4*diag(domain(:,2)-domain(:,1),0)*randn(N_variables,N_particules);
    Essaim(:,Mut) = Essaim(:,Mut) + Perturbation(:,Mut);
    Vitesses(:,Mut) = zeros(N_variables,sum(Mut));
    % G�n�rer nouvel essaim
    Essaim = Essaim + Vitesses;
    
    % Forcer les particules rester dans les domaines de variation
    [Essaim] = forceDomain(Essaim,domain);
    % Evaluation de l'essaim
    [Objectifs,Contraintes,Divers] = feval(fonction,Essaim);
    % Mise � jour des m�moires individuelles
    [Memoires_Variables,Memoires_Objectifs,Memoires_Contraintes,Somme_Viols_Memoires] ...
        = MAJ_Memoires(Essaim,Objectifs,Contraintes,Memoires_Variables,Memoires_Objectifs,Memoires_Contraintes);
    % Mise � jour du front de Pareto
    if length(Objectifs(:,1)) == 2
        [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] ...
            = extraire_front([Essaim Front_Pareto_Variables],[Objectifs Front_Pareto_Objectifs],[Contraintes Front_Pareto_Contraintes],[Divers Front_Pareto_Divers]);
    elseif length(Objectifs(:,1)) == 3
        [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] ...
            = extraire_front_3obj([Essaim Front_Pareto_Variables],[Objectifs Front_Pareto_Objectifs],[Contraintes Front_Pareto_Contraintes],[Divers Front_Pareto_Divers]);
    else
        disp('Not supporting more than 3 objectives. Will be upgraded soon!')
        return
    end
    % Ajustement de l'archive
    [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers,Distance]...
        = ajuster_archive(Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers,N_archive);
    
    % Determination des coordonn�es des points extr�mes du Front de Pareto
    if isempty(Front_Pareto_Objectifs)
        Extremes(:,cycle) = NaN(length(Objectifs(:,1)),1);
    else
        Extremes(:,cycle) = min(Front_Pareto_Objectifs,[],2);
    end
    Taille_archive(cycle) = length(Front_Pareto_Variables(1,:));
    % Generation saving
    OBJpareto(1:size(Front_Pareto_Objectifs,1),1:size(Front_Pareto_Objectifs,2),cycle) = Front_Pareto_Objectifs;
    OBJmemory(1:size(Memoires_Objectifs,1),1:size(Memoires_Objectifs,2),cycle) = Memoires_Objectifs;
    VARpareto(1:size(Front_Pareto_Variables,1),1:size(Front_Pareto_Variables,2),cycle) = Front_Pareto_Variables;
    VARmemory(1:size(Memoires_Variables,1),1:size(Memoires_Variables,2),cycle) = Memoires_Variables;
    CONSTRpareto(1:size(Front_Pareto_Contraintes,1),1:size(Front_Pareto_Contraintes,2),cycle) = Front_Pareto_Contraintes;
    CONSTRmemory(1:size(Memoires_Contraintes,1),1:size(Memoires_Contraintes,2),cycle) = Memoires_Contraintes;
    MISCpareto(1:size(Front_Pareto_Divers,1),1:size(Front_Pareto_Divers,2),cycle) = Front_Pareto_Divers;
    
    if ~isempty(Front_Pareto_Objectifs)
        cycle = cycle + 1;
    else
        cycle_temp = cycle_temp + 1;
    end
     
    
    % Option to stop if no bigger change (1%) has been made in all
    % objective dimensions)
    if Option.paretoCheck(1)
        if cycle > 1                                                        % If there is a pareto front
            obj = zeroTruncate(Front_Pareto_Objectifs);
            temp = sum(obj,2)/size(obj,2);
            y(:,cycle - 1) = temp;
            if cycle > 2
                errorCheck = (temp - y(:,cycle - 2))./temp*100;
                if all(abs(errorCheck) < Option.paretoCheck(1)*ones(size(y,1),1))
                    nImprove = nImprove + 1;
                else
                    nImprove = 0;                                           % Reset nImprove
                end
            end
        end
    end
end



function [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers,Distance]...
    = ajuster_archive(Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers,N_archive)

% Triple number of archives if more than 3 objectives presented
if size(Front_Pareto_Objectifs,1) >= 3
    N_archive = N_archive*3;
end

[poubelle,Indices] = sort(Front_Pareto_Objectifs(1,:),'ascend');
Front_Pareto_Variables = Front_Pareto_Variables(:,Indices);
Front_Pareto_Objectifs = Front_Pareto_Objectifs(:,Indices);
Front_Pareto_Contraintes = Front_Pareto_Contraintes(:,Indices);
Front_Pareto_Divers=Front_Pareto_Divers(:,Indices);

if isempty(Indices)
    Distance = [];
    return
elseif length(Indices) == 1
    Distance = 1;
elseif length(Indices) == 2
    Distance = [1 1];
else
    Nadir=max(Front_Pareto_Objectifs,[],2)';
    Ecart(1,:) = Nadir(1)-Front_Pareto_Objectifs(1,:);
    Ecart(1,:) = Ecart(1,:)/max(Ecart(1,:));
    Ecart(2,:) = Nadir(2)-Front_Pareto_Objectifs(2,:);
    Ecart(2,:) = Ecart(2,:)/max(Ecart(2,:));
    Angle = atan(Ecart(2,:)./Ecart(1,:))*180/pi;
    Angle = repmat(Angle,N_archive,1);
    Angle_souhaite = linspace(0,90,N_archive);
    Angle_souhaite = repmat(Angle_souhaite',1,length(Angle(1,:)));
    Distance = abs(Angle_souhaite-Angle);
    [Minimums,Indices] = min(Distance,[],2);
end

Indices = unique(Indices);

Front_Pareto_Variables = Front_Pareto_Variables(:,Indices);
Front_Pareto_Objectifs = Front_Pareto_Objectifs(:,Indices);
Front_Pareto_Contraintes = Front_Pareto_Contraintes(:,Indices);
Front_Pareto_Divers = Front_Pareto_Divers(:,Indices);
Distance = Distance(:,Indices);

function Guides = choisir_guides(N_particules,Objectifs,Front_Pareto_Objectifs,Somme_Viols_Memoires,Guides,Mut)

if isempty(Front_Pareto_Objectifs(1,:))
    % Si aucun individu ne respectent les contraintes, on choisit de suivre
    % celui qui minimise la somme normalis饠des contraintes.
    [toto,indice] = min(Somme_Viols_Memoires);
    Guides=indice*ones(1,N_particules);
    return
end

if length(Front_Pareto_Objectifs(1,:))==1
    Guides = ones(1,N_particules);
    return
end

% 
Guides=round(linspace(1,length(Front_Pareto_Objectifs(1,:)),N_particules));

function [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] ...
    = extraire_front(Essaim,Objectifs,Contraintes,Divers)

% Enlever les particules violant au moins une contrainte
Somme = sum(max(Contraintes,0),1);
indices = find(Somme>0);
Essaim(:,indices) = [];
Objectifs(:,indices) = [];
Contraintes(:,indices) = [];
Divers(:,indices) = [];

% Trier les particules en ordre croissant par rapport ࠬ'objectif 1
[poubelle,Indices] = sort(Objectifs(1,:),'ascend');
Essaim = Essaim(:,Indices);
Objectifs = Objectifs(:,Indices);
Contraintes = Contraintes(:,Indices);
Divers = Divers(:,Indices);

N=length(Essaim(1,:));

% Si tous les individus violent les contraintes alors renvoyer des vecteurs
% vides
if N==0
    Front_Pareto_Variables = Essaim;
    Front_Pareto_Objectifs = Objectifs;
    Front_Pareto_Contraintes = Contraintes;
    Front_Pareto_Divers = Divers;
    return
end

% Appeller la fonction de Tri
[Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] ...
    = sous_fonction_recursive(Essaim,Objectifs,Contraintes,Divers);

return

function [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] ...
    = sous_fonction_recursive(Essaim,Objectifs,Contraintes,Divers)

% condition d'arr괠: Si l'appel ce la fonction se fait avec une population
% de taille 1, alors renvoyer cette particule

N = length(Essaim(1,:));
if N == 1
    Front_Pareto_Variables = Essaim;
    Front_Pareto_Objectifs = Objectifs;
    Front_Pareto_Contraintes = Contraintes;
    Front_Pareto_Divers = Divers;
    return
end

% Sinon appeller cette fonction sur deux moiti鳠de la population
[Partie_haute,Partie_haute_Objectifs,Partie_haute_Contraintes,Partie_haute_Divers] ...
    = sous_fonction_recursive(Essaim(:,1:floor(N/2)),Objectifs(:,1:floor(N/2)),Contraintes(:,1:floor(N/2)),Divers(:,1:floor(N/2)));
[Partie_basse,Partie_basse_Objectifs,Partie_basse_Contraintes,Partie_basse_Divers] ...
    = sous_fonction_recursive(Essaim(:,floor(N/2)+1:end),Objectifs(:,floor(N/2)+1:end),Contraintes(:,floor(N/2)+1:end),Divers(:,floor(N/2)+1:end));

% Rassembler les deux fronts de Pareto resultants
Test = Partie_basse_Objectifs(2,:) < Partie_haute_Objectifs(2,end);
Indice = find(Test,1);

Test2 = 0;
if ~isempty(Indice)
    Test2 = Partie_basse_Objectifs(1,Indice) == Partie_haute_Objectifs(1,end);
end

Front_Pareto_Variables = [Partie_haute(:,1:end-1*Test2) Partie_basse(:,Indice:end)];
Front_Pareto_Objectifs = [Partie_haute_Objectifs(:,1:end-1*Test2) Partie_basse_Objectifs(:,Indice:end)];
Front_Pareto_Contraintes = [Partie_haute_Contraintes(:,1:end-1*Test2) Partie_basse_Contraintes(:,Indice:end)];
Front_Pareto_Divers = [Partie_haute_Divers(:,1:end-1*Test2) Partie_basse_Divers(:,Indice:end)];
return

function [Memoires_Variables,Memoires_Objectifs,Memoires_Contraintes,Somme_Viols_Memoires] ...
        = MAJ_Memoires(Essaim,Objectifs,Contraintes,Memoires_Variables,Memoires_Objectifs,Memoires_Contraintes)

% Si la nouvelle position a un Somme_Viols plus faible alors elle devient
% la m魯ire
N_particules = length(Essaim(1,:));    
Contraintes_totales = [Contraintes Memoires_Contraintes];
Contraintes_max = max(Contraintes_totales,[],2);
Somme_Viols = sum(diag(1./Contraintes_max(Contraintes_max > 0),0) * max( Contraintes_totales(Contraintes_max > 0,:) , 0 ),1);

Somme_Viols_Essaim = Somme_Viols(1:N_particules);
Somme_Viols_Memoires = Somme_Viols(N_particules + 1:2*N_particules);

indices = Somme_Viols_Memoires > Somme_Viols_Essaim;

Memoires_Variables(:,indices) = Essaim(:,indices);
Memoires_Objectifs(:,indices) = Objectifs(:,indices);
Memoires_Contraintes(:,indices) = Contraintes(:,indices);

% Si la m魯ire est domin饠:
condition = Somme_Viols_Memoires + Somme_Viols_Essaim == 0;
condition = condition & Memoires_Objectifs(1,:) >= Objectifs(1,:) ...
        & Memoires_Objectifs(2,:) >= Objectifs(2,:);
indices = find(condition);
Memoires_Variables(:,indices) = Essaim(:,indices);
Memoires_Objectifs(:,indices) = Objectifs(:,indices);
Memoires_Contraintes(:,indices) = Contraintes(:,indices);

% Si la m魯ire est non domin�?
condition = Somme_Viols_Memoires + Somme_Viols_Essaim == 0;
condition = condition ...
    &~(Memoires_Objectifs(1,:) >= Objectifs(1,:) & Memoires_Objectifs(2,:) >= Objectifs(2,:))...
    &~(Memoires_Objectifs(1,:) <= Objectifs(1,:) & Memoires_Objectifs(2,:) <= Objectifs(2,:));
indices = find(condition);
Memoires_Variables(:,indices) = Essaim(:,indices);
Memoires_Objectifs(:,indices) = Objectifs(:,indices);
Memoires_Contraintes(:,indices) = Contraintes(:,indices);

function [Front_Pareto_Variables,Front_Pareto_Objectifs,Front_Pareto_Contraintes,Front_Pareto_Divers] = ...
    extraire_front_3obj(Essaim,Objectifs,Contraintes,Divers)

% Enlever les particules violant au moins une contrainte
Somme = sum(max(Contraintes,0),1);
indices = find(Somme>0);
Essaim(:,indices) = [];
Objectifs(:,indices) = [];
Contraintes(:,indices) = [];
Divers(:,indices) = [];

% Trier les particules en ordre croissant par rapport � l'objectif 1
[poubelle,Indices] = sort(Objectifs(1,:),'ascend');
Essaim = Essaim(:,Indices);
Objectifs = Objectifs(:,Indices);
Contraintes = Contraintes(:,Indices);
Divers = Divers(:,Indices);

N = length(Essaim(1,:));

% Si tous les individus violent les contraintes alors renvoyer des vecteurs
% vides
if N == 0
    Front_Pareto_Variables = Essaim;
    Front_Pareto_Objectifs = Objectifs;
    Front_Pareto_Contraintes = Contraintes;
    Front_Pareto_Divers = Divers;
    return
end

% Trouver les particules non domin�es
Front_Pareto_Variables = Essaim;
Front_Pareto_Objectifs = Objectifs;
Front_Pareto_Contraintes = Contraintes;
Front_Pareto_Divers = Divers;

j = 1;
while j <= length(Front_Pareto_Objectifs(1,:))
    % Trouver les particules domin�es au sens large par la particule j
    condition = Front_Pareto_Objectifs(1,:) >= Front_Pareto_Objectifs(1,j) ...
        & Front_Pareto_Objectifs(2,:) >= Front_Pareto_Objectifs(2,j) ...
        & Front_Pareto_Objectifs(3,:) >= Front_Pareto_Objectifs(3,j);
    indices = find(condition);
    indices(indices==j) = [];
    %Supprimer ces particules domin�es
    Front_Pareto_Variables(:,indices) = [];
    Front_Pareto_Objectifs(:,indices) = [];
    Front_Pareto_Contraintes(:,indices) = [];
    Front_Pareto_Divers(:,indices) = [];

    if isempty(find(indices < j,1))
        j = j + 1;
    else
        j = min(indices);
    end
end