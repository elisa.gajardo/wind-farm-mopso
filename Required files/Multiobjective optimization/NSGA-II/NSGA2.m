% The NSGA-II multi-objective genetic algorithm w/ 
% simulated binary crossover (SBX) and polynomial mutation.
%
% ARGUMENTS
%
% nmbOfIndivs - number of individuals in each generation
% nmbOfGens - number of generations for the algorithm to run
% Fct - objective function handle (i.e. @FunctionName)
%    Input :
%      Indivs - Population to evaluate
%         MxN array where M = number of decision variables and N = number
%         of individuals
%    Outputs :
%       ObjVals - Objectives values for each individual
%         MxN array where M = number of objectives and N = number of
%         individuals
%       ConstrVals - constraints values for each individuals in the form :
%       g_i(x) <= 0
%         MxN array where M = number of constraints and N = number of
%         individuals
% RandParams - parameters for the random initialization of the initial 
%   (zero-th) generation (see NSGA_ParentGen0.m)
% Ranges - permissible range for each decision variable
% pX - crossover probability/fraction (see NSGA_SBX.m)
% etaX - distribution parameter for simulated binary crossover 
%   (SBX; see NSGA_SBX.m)
% pM - mutation probability/fraction (see NSGA_Mutate.m)
% etaM - distribution parameter for polynomial mutation (see NSGA_Mutate.m)
% Options - structure containing the following fields :
%           SaveDir : save directory (each generation is saved)
%           ParGen  : initial generation, if not set, the initial
%           generation is created using RandParams.
%         This argument is optional (default is no save and random initial
%         generation). Each field is also optional : if the field is not
%         present the default behaviour is obtained.
%
% RETURN VALUES
%
% ParGen - final (parent) generation
% ObjVals - objective function values for each individual of the final 
%   generation
% Ranking - ranks of the individuals of the final generation
% SumOfViols - sum of constraint violations for each individual of the 
%   final generation
% NmbOfFront - number of pareto-optimal front for each individual of the 
%   final generation
%
% Modified by G. Robin - 28/09/2004
%   - Renamed Cons as Ranges
%   - Added calculated constraints handling : user function now returns a
%   constraints matrix ConstrVals (Nc by N matrix where Nc is the number of
%   constraints and N the number of individuals) along with the objectives
%   matrix ObjVals.
%   - Bounds is suppressed : upper and lower bound are dynamically computed
%   within each Front during the Crowdist calculation
%   - User defined objective function is passed via a handle
%   - Added possibility to supply initial generation instead of randomly
%   generating it
%   - Mutation is made conform to the Non-Uniform mutation algorithm
%   described by Deb (maxM input is suppressed).
%
% Copyright (C) 2004 Reiner Schulz (rschulz@cs.umd.edu)
% This file is part of the author's Matlab implementation of the NSGA-II
% multi-objective genetic algorithm with simulated binary crossover (SBX)
% and polynomial mutation operators.
%
% The algorithm was developed by Kalyanmoy Deb et al. 
% For details, refer to the following article:
% Deb, K., Pratap, A., Agarwal, S., and Meyarivan, T. 
% A fast and elitist multiobjective genetic algorithm: NSGA-II.
% in IEEE TRANSACTIONS ON EVOLUTIONARY COMPUTATION, vol. 8, pp. 182-197, 2002
%
% This implementation is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This implementation is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with This implementation; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [VARpareto, OBJpareto] = NSGA2(Option)
clc
saveFile = 'Result_NSGA2';
fid = fopen([saveFile,'.txt'],'w');
startTime = datestr(datetime('now'));
fprintf('Program has been launched at:\n');
fprintf('\n');
fprintf('%s ... >>\n',startTime);
fprintf('\n');

% Write to file
fprintf(fid,'Program has been launched at:\n');
fprintf(fid,'\n');
fprintf(fid,'%s ... >>\n',startTime);
fprintf(fid,'\n');
domain = Option.domain;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters setting by default
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pX = Option.StraParam.pX;
etaX = Option.StraParam.etaX;
pM = Option.StraParam.pM;
etaM = Option.StraParam.etaM;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nIndividual = Option.nIndividual;

if exist([saveFile,'.mat'],'file')
    delete([saveFile,'.mat'])                                               % Delete result file if existed       
end

if ~Option.reload
    if ~isfield(Option, 'ParGen')                                           % If no parent generation given
        % Intialize random number generator to an arbitrary state using clock
        rand('state',sum(100*clock));
        % Create initial (parent) generation
              % Forced to domain included
        ParGen = x0Gen(domain,nIndividual,Option.samplingMethod);
    else
        %Check suppled initial generation for coherency with the parameters
        ParGen = Option.ParGen;
        [foo,bar] = size(ParGen);
        [NmbOfVars,dump] = size(domain);
        if (foo~=NmbOfVars) || (bar~=nIndividual)
            error('Initial generation not coherent with either nmbOfIndivs or Ranges')
        end
    end
    
    [ObjVals, ConstrVals, MiscVals] = feval(Option.Fcn, ParGen);
        
    % Rank individuals of initial generation
    [Ranking, SumOfViols, NmbOfFront, foo] = NSGA_Rank(ParGen, ObjVals, ConstrVals);
    
    nImprove = 1;                                                           % Pareto quality check counter
    % Repeat for given number of generations
    cycle = 1;

else
    load BackUp_NSGA2 -regexp ^(?!Option$).                                 % Load recovery file except Option variable
end

splitLine = repmat('-',1,35);
fprintf('%s\n',splitLine);
fprintf('|  %10s  |  %10s |\n','Generation','Duration (mins)');
fprintf('%s\n',splitLine);

fprintf(fid,'%s\n',splitLine);
fprintf(fid,'|  %10s  |  %10s |\n','Generation','Duration (mins)');
fprintf(fid,'%s\n',splitLine);
feasibilityCheck = 1;
while (cycle <= Option.nIteration && nImprove <= Option.paretoCheck(2) && feasibilityCheck <= Option.maxTrial)
    startTime = datetime('now');
    % Create child generation
    [ChildGen foo] = NSGA_ChildGen(ParGen, Ranking, domain, pX, etaX, pM, etaM);
    % Force variable into domain with step
    ChildGen = forceDomain(ChildGen,domain);
    % append children to parents
    Par_ChildGen = [ParGen ChildGen];
    % Append objective function and constraints values of the children to
    % those of the parents
    
    [ChildObjVals, ChildConstrVals, ChildMiscVals]= feval(Option.Fcn, ChildGen);
    ObjVals = [ObjVals ChildObjVals];
    ConstrVals = [ConstrVals ChildConstrVals];
    MiscVals = [MiscVals ChildMiscVals];
    
    feasibleMemory(cycle) = feasibleCheck(ConstrVals);
    
    if ~feasibleCheck(ConstrVals)
        feasibilityCheck = feasibilityCheck + 1;
    end

    % Rank children and parents together
    [Ranking, SumOfViols, NmbOfFront, foo] = NSGA_Rank(Par_ChildGen, ObjVals, ConstrVals);
    % Ranks of the parents
    ParRanking = Ranking(1:nIndividual);
    % Elite parents that will not be replaced by children
    ElitePars = find(ParRanking <= nIndividual);
    % Non-elite parents that will be replaced by children
    NonElitePars = find(ParRanking > nIndividual);
    % Ranks of the children
    
    ChildRanking = Ranking((nIndividual + 1):end);
    % Children that will replace worse parents
    BetterChildren = find(ChildRanking <= nIndividual);
    
    % Replace non-elite parents with better children
    ParGen(:,NonElitePars) = ChildGen(:,BetterChildren);
    % Use the same type of `filling-in-the-gaps-between-elite-parents' for obj. val.s etc.
    % to keep indices in sync w/ run numbers
    % ChildObjVals = ObjVals( :, (nmbOfIndivs + 1):end);                  	% children
    % Patch G.Robin -> not needed anymore
    ObjVals = ObjVals(:,1:nIndividual); % parents
    ObjVals(:,NonElitePars) = ChildObjVals(:,BetterChildren);             	% replace non-elite parents
    % Patch G.Robin -> Added selecting the constraints values
    % ChildConstrVals = ConstrVals( :, (nmbOfIndivs + 1):end); % children
    % Patch G.Robin -> not needed here
    ConstrVals = ConstrVals(:,1:nIndividual);                               % parents
    ConstrVals(:,NonElitePars) = ChildConstrVals(:,BetterChildren);         % replace non-elite parents
    
    MiscVals = MiscVals(:,1:nIndividual);                                   % parents
    MiscVals(:,NonElitePars) = ChildMiscVals(:,BetterChildren);
    
    
    ChildSumOfViols = SumOfViols( (nIndividual + 1):end);                   % children
    SumOfViols = SumOfViols(1:nIndividual); % parents
    SumOfViols(NonElitePars) = ChildSumOfViols(BetterChildren);             % replace non-elite parents
    ChildNmbOfFront = NmbOfFront( (nIndividual + 1):end);                   % children
    NmbOfFront = NmbOfFront( 1:nIndividual); % parents
    NmbOfFront(NonElitePars) = ChildNmbOfFront(BetterChildren);             % replace non-elite parents
    
    % Have ChildRanking already
    Ranking = ParRanking; % parents
    Ranking(NonElitePars) = ChildRanking(BetterChildren);                   % replace non-elite parents
    
    [OBJsort,VARsort,CONSTRsort] = paretoFront(ObjVals,0,ParGen,ConstrVals);
    [OBJsort,VARsort,CONSTRsort] = paretoSort(OBJsort,VARsort,CONSTRsort); 	% Sorting pareto-front with increasing first objective

    [OBJsortTemp,VARsortTemp,MISCsort] = paretoFront(ObjVals,0,ParGen,MiscVals);
    [~,~,MISCsort] = paretoSort(OBJsortTemp,VARsortTemp,MISCsort); 
    
    OBJpareto(1:size(OBJsort,1),1:size(OBJsort,2),cycle) = OBJsort;
    VARpareto(1:size(VARsort,1),1:size(VARsort,2),cycle) = VARsort;
    CONSTRpareto(1:size(CONSTRsort,1),1:size(CONSTRsort,2),cycle) = CONSTRsort;
    MISCpareto(1:size(MISCsort,1),1:size(MISCsort,2),cycle) = MISCsort;
    endTime = datetime('now');
    timeNow = minutes(endTime - startTime);
    fprintf('|  %6d      |      %6.2f      |\n', cycle,timeNow);
    fprintf(fid,'|  %6d      |      %6.2f      |\n', cycle,timeNow);
    
    cycle = cycle + 1;
    
    % Plot result at every generation if wished
    lastParetoObj = OBJsort;
    lastParetoVar = VARsort;
    lastParetoMisc = MISCsort;
    if Option.plotDisplay
        objectiveDisplay(OBJsort);
    end
    
    % Pareto front improvement check, if no improvement after a certain
    % consecutive generations, optimization will stop
    if Option.paretoCheck(1)
        if cycle > 1
            obj = zeroTruncate(ObjVals);
            temp = sum(obj,2)/size(obj,2);
            y(:,cycle - 1) = temp;
            if cycle > 2
                errorCheck = (temp - y(:,cycle - 2))./temp*100;
                if all(abs(errorCheck) < Option.paretoCheck(1)*ones(size(y,1),1))
                    nImprove = nImprove + 1;
                else
                    nImprove = 0;                                           % Reset nImprove
                end
            end
        end
    end
        
    % Check number of variable touching its lower boundary
    [touchingVar,touchingBound,touchingRep] = boundaryTouchingCheck(Option,VARsort);
    drawnow;
    Touching.var = touchingVar;
    Touching.bound = touchingBound;
    Touching.rep = touchingRep;
    save('BackUp_NSGA2')                                                    % Back up result if an error happened
    save(saveFile,'lastParetoObj','lastParetoVar','lastParetoMisc','Option','OBJpareto','VARpareto','CONSTRpareto','MISCpareto','feasibleMemory','Touching')
end
fprintf('%s\n',splitLine);
fprintf(fid,'%s\n',splitLine);
endTime = datestr(datetime('now'));
fprintf('Completed at: %s\n',endTime);
fprintf(fid,'Completed at: %s\n',endTime);
fprintf('\n');
fclose(fid);
feasibleConclusion = feasibilityCheck <= Option.maxTrial;                   % To conslude optimization feasibility
save(saveFile,'lastParetoObj','lastParetoVar','lastParetoMisc','Option','OBJpareto','VARpareto','CONSTRpareto','MISCpareto','feasibleConclusion','feasibleMemory','Touching')

% Export option to excel file
if Option.exportToExcel
    exportToExcel(OBJpareto,VARpareto,CONSTRpareto,MISCpareto,saveFile);
end