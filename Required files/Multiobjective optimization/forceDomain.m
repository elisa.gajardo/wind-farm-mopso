% Function to force variable domain into the specified domain (with step)
function [Variable] = forceDomain(Variable, Domain)

N_particle = size(Variable,2);

% Shift everything by lower bounds of the domain
tempSave = repmat(Domain(:,1),1,N_particle);                                % Save lower bounds
Variable = Variable - tempSave;                                             % Variable shift
Domain(:,1:2) = Domain(:,1:2) - repmat(Domain(:,1),1,2);

% Dealing with discrete steps
for i = 1:size(Variable,1)
    if Domain(i,3) ~= 0
        Ecart = mod(Variable(i,:),Domain(i,3));
        Variable(i,:) = Variable(i,:) - Ecart + Domain(i,3)*ones(1,N_particle).*...
                        (Ecart/Domain(i,3) > rand(1,N_particle));
    end
end

% Force variable fall in the range between lower and upper bounds
Variable = max(repmat(Domain(:,1),1,N_particle),Variable);                  % Make sure variable not less than lower bound
Variable = min(repmat(Domain(:,2),1,N_particle),Variable);                  % Make sure variable not bigger than upper bound

Variable = Variable + tempSave;                                             % Shift back variable
