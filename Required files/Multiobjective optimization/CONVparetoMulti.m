% Function to examine the convergence trend of a objective and variable
% throughout the iteration. This function is more meaningful in case of
% mono-objective optimization.
% Input
% - checkOption: 'var', 'var' or 'misc': to check objective or variable
% - objectPos: order of variable or misc.
% Note: plot against the first objective
function CONVparetoMulti(checkOption,objectPos,lastParetoObj,lastParetoVar,lastParetoMisc,saveOption)
nVar = size(lastParetoVar,1);
nMisc = size(lastParetoMisc,1);
paretoObjPlot = lastParetoObj(1,:);
if strcmpi(checkOption,'var')
    if max(objectPos) > nVar
        errordlg('Exceeds number of variables')
        error('Exceeds number of variables')       
    end
    VAR = lastParetoVar(objectPos,:);
    plot(paretoObjPlot,VAR,'*')
    xlabel('Objective 1')
	ylabel(['Variable ',num2str(objectPos)]);
	grid on
    if saveOption
        save(['Variable_',num2str(objectPos)],'VAR','paretoObjPlot')
    end
elseif strcmpi(checkOption,'misc')
    if max(objectPos) > nMisc
        errordlg('Exceeds number of misc.')
        error('Exceeds number of misc.')       
    end
    MISC = lastParetoMisc(objectPos,:);
    
    plot(paretoObjPlot,MISC,'*')
    xlabel('Objective 1')
	ylabel(['Misc. ',num2str(objectPos)]);
	grid on
    if saveOption
        save(['Misc_',num2str(objectPos)],'MISC','paretoObjPlot')
    end
end