% Function to examine the convergence trend of a objective and variable
% throughout the iteration. This function is more meaningful in case of
% mono-objective optimization.
% Input
% - pointPos: position of the point
% - checkOption: 'obj', 'var' or 'misc': to check objective or variable
% convergences
% - objectPos: list of position to check (either objective order or
% variable order, accordingly)
% - saveOption: to save data or not
function CONVparetoMono(pointPos,checkOption,objectPos,OBJpareto,VARpareto,MISCpareto,saveOption)
nIter = size(OBJpareto,3);                                                  % Number of iterations
nObj = size(OBJpareto,1);
nVar = size(VARpareto,1);
nMisc = size(MISCpareto,1);

objEnd = OBJpareto(:,:,end);
%-----------------------------------------------------------
% With mono-objective optimization
%-----------------------------------------------------------
if objEnd(1,1) == objEnd(2,1) && size(OBJpareto,2) == 1
    % Objectives
    if strcmpi(checkOption,'obj')
        if max(objectPos) > nObj
            error('Exceeds number of objectives')
        end
        
        for j = 1:nIter
            OBJ(j) = OBJpareto(objectPos,pointPos,j);
        end
        plot(OBJ,'*-')
        xlabel('Iteration')
        ylabel(['Objective ',num2str(objectPos)]);
        grid on
        if saveOption
            save(['OBJ',num2str(objectPos)],'OBJ')
        end
        % Variables
    elseif strcmpi(checkOption,'var')
        if max(objectPos) > nVar
            errordlg('Exceeds number of variables')
            error('Exceeds number of variables')
        end
        for j = 1:nIter
            VAR(j) = VARpareto(objectPos,pointPos,j);
        end
        plot(VAR,'*-')
        xlabel('Iteration')
        ylabel(['Varible ',num2str(objectPos)]);
        grid on
        if saveOption
            save(['Variable_',num2str(objectPos)],'VAR')
        end
        % Misc. value
    elseif strcmpi(checkOption,'misc')
        if max(objectPos) > nMisc
            error('Exceeds number of assigned misc. values')
        end
        for j = 1:nIter
            MISC(j) = MISCpareto(objectPos,pointPos,j);
        end
        plot(MISC,'*-')
        xlabel('Iteration')
        ylabel(['Misc. ',num2str(objectPos)]);
        grid on
        if saveOption
            save(['Misc_',num2str(objectPos)],'MISC')
        end
    end
end
