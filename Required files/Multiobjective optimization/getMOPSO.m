% Function to retrieve variable, objective and constraint values for the
% last iteration in MOPSO and also plot again the final pareto front
% Input
% - n: Iteration number wants to take out the data or 'end'
% - truncate: option to remove all zero columns (1 if wish)
% - plotDisplay; [optional] to plot again the pareto front
% Note: Temporarily for a two objective problem

function [VARpareto, OBJpareto, CONSTRpareto] = getMOPSO(n,truncate,plotDisplay)
load Result_MOPSO

if strcmp(n,'end')
    n = size(OBJpareto,3);
end

VARpareto = VARpareto(:,:,n);                                               % Variables
OBJpareto = OBJpareto(:,:,n);                                               % Objectives
CONSTRpareto = CONSTRpareto(:,:,n);                                         % Constraint

if truncate == 1
    VARpareto = zeroTruncate(VARpareto);
    OBJpareto = zeroTruncate(OBJpareto);
    CONSTRpareto = zeroTruncate(CONSTRpareto);
end

figure(1)
if size(OBJpareto,1) == 2
    plot(OBJpareto(1,:),OBJpareto(2,:),'o','markersize',6,'markerfacecolor','b');
    grid
    xlabel('Objective 1')
    ylabel('Objective 2')
else
    plot3(OBJpareto(1,:),OBJpareto(2,:),OBJpareto(3,:),'o','markersize',6,'markerfacecolor','b');
    grid
    xlabel('Objective 1')
    ylabel('Objective 2')
    zlabel('Objective 3')
end

if size(OBJpareto,2) == 3
    figure(2)
    subplot(1,3,1)
    plot(OBJpareto(1,:),OBJpareto(2,:),'o','markersize',6,'markerfacecolor','b');
    grid
    xlabel('Objective 1')
    ylabel('Objective 2')
    
    subplot(1,3,2)
    plot(OBJpareto(1,:),OBJpareto(3,:),'o','markersize',6,'markerfacecolor','b');
    grid
    xlabel('Objective 1')
    ylabel('Objective 3')
    
    subplot(1,3,3)
    plot(OBJpareto(2,:),OBJpareto(3,:),'o','markersize',6,'markerfacecolor','b');
    grid
    xlabel('Objective 2')
    ylabel('Objective 3')
    drawnow
end