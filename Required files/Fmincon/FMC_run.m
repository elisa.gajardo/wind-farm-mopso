% Function to run mono-objective function with constraint using Fmincon
% Input
% - maxTrial: maximum number of trials to find a feasible starting point
% - maxIter: maximum number of iterations
% - maxFeval: maximum number of function evaluations
% - tolFun: function terleance for convergence
% - UserInput: any user input to modify function
% - displayOpt: to display process or not
% - saveOpt: save result option
function [x,fval,exitFlag,output] = FMC_run(maxTrial,maxIter,maxFeval,UserInput,displayOpt,saveOpt)

[domain, x0, A, B, UserInput] = domainFMC(maxTrial, UserInput);             % Design domain and find feasible starting point

if ~isempty(x0)
    Option = struct(...
        'maxIter',          maxIter,...                                     % Maximum number of iterations
        'maxFeval',        	maxFeval,...                                    % Maximum number of function evaluation
        'tolFun',           1e-2,...                                        % Function evaluation tolerance
        'tolX',             1e-2,...                                        % Variable tolerance
        'tolCon',           1e-2,...                                        % Constraint tolerance
        'parallel',         false,...                                       % Parallel computing
        'Fcn',              @(x) myFunFMC(x,UserInput),...                  % Optimization function
        'algorithm',        'sqp',...                                       % Algorithm: sqp, active-set, iterior-point
        'solver',           'FMSC',...                                      % Fmincon or fminsearchcon (implemented version of fminsearch) solver
        'display',          displayOpt,...           % Display plot
        'save',             saveOpt);                                       % Saving option
    
    Option = var2struct(Option,domain,x0,A,B);
    [x, fval, exitFlag, output] = FMC(Option);
else
    x = zeros(size(domain,1),1);
    fval = 1e6;
    exitFlag = -1;
    output = [];
end