function varargout = FMCopt(varargin)
% FMCOPT MATLAB code for FMCopt.fig
%      FMCOPT, by itself, creates a new FMCOPT or raises the existing
%      singleton*.
%
%      H = FMCOPT returns the handle to a new FMCOPT or the handle to
%      the existing singleton*.
%
%      FMCOPT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FMCOPT.M with the given input arguments.
%
%      FMCOPT('Property','Value',...) creates a new FMCOPT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FMCopt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FMCopt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FMCopt

% Last Modified by GUIDE v2.5 10-Sep-2019 11:46:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FMCopt_OpeningFcn, ...
                   'gui_OutputFcn',  @FMCopt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FMCopt is made visible.
function FMCopt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FMCopt (see VARARGIN)

% Choose default command line output for FMCopt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FMCopt wait for user response (see UIRESUME)
% uiwait(handles.FMCopt);


% --- Outputs from this function are returned to the command line.
function varargout = FMCopt_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function maxFeval_Callback(hObject, eventdata, handles)
% hObject    handle to maxFeval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxFeval as text
%        str2double(get(hObject,'String')) returns contents of maxFeval as a double


% --- Executes during object creation, after setting all properties.
function maxFeval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxFeval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tolFun_Callback(hObject, eventdata, handles)
% hObject    handle to tolFun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tolFun as text
%        str2double(get(hObject,'String')) returns contents of tolFun as a double


% --- Executes during object creation, after setting all properties.
function tolFun_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tolFun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tolX_Callback(hObject, eventdata, handles)
% hObject    handle to tolX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tolX as text
%        str2double(get(hObject,'String')) returns contents of tolX as a double


% --- Executes during object creation, after setting all properties.
function tolX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tolX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tolCon_Callback(hObject, eventdata, handles)
% hObject    handle to tolCon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tolCon as text
%        str2double(get(hObject,'String')) returns contents of tolCon as a double


% --- Executes during object creation, after setting all properties.
function tolCon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tolCon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in parPool.
function parPool_Callback(hObject, eventdata, handles)
% hObject    handle to parPool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of parPool


% --- Executes on selection change in optAlgorithm.
function optAlgorithm_Callback(hObject, eventdata, handles)
% hObject    handle to optAlgorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns optAlgorithm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from optAlgorithm


% --- Executes during object creation, after setting all properties.
function optAlgorithm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to optAlgorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist('myFunFMC.m','file') && exist('domainFMC.m','file')
    % Parallel computing options
    parPoolCheck = get(handles.parPool,'Value');
    if parPoolCheck
        parPoolRun(2);
    else
        delete(gcp('nocreate'));
    end
    
    % Get input from edit text box
    maxFeval = str2num(get(handles.maxFeval,'String'));
    tolFun = str2num(get(handles.tolFun,'String'));
    tolX = str2num(get(handles.tolX,'String'));
    maxTrial = str2num(get(handles.maxTrial,'String'));
    maxIter = str2num(get(handles.maxIter,'String'));
    % optalgorithm choosing
    optAlgorithm = get(handles.optAlgorithm,'Value');
    
    switch optAlgorithm
        case 1
            fminconAlg = 'sqp';
        case 2
            fminconAlg = 'active-set';
        case 3
            fminconAlg = 'interior-point';
    end
        
    [domain, x0, A, B] = domainFMC(maxTrial);                               % Design domain
    Option = struct(...
        'maxFeval',       	maxFeval,...                                    % Maximum number of function evaluations
        'tolFun',           tolFun,...                                     	% Function evaluation tolerance
        'tolX',             tolX,...                                        % Variable tolerance
        'maxIter',          maxIter,...                                     % Maximum number of iterations
        'Fcn',              @(x) myFunFMC(x),...                            % Optimization function
        'parallel',         parPoolCheck,...                                % Parallel computing
        'algorithm',        fminconAlg,...                                  % Fmincon algorithm
        'display',          true,...                                        % Display plot;                                    
        'save',             true);                                          % Saving option
    
    Option = var2struct(Option,domain,x0,A,B);
    Option.solver = 'FMC';                                                  % Solver
    set(hObject,'String','Running')
    set(handles.optStatus,'String','Waiting is happiness')              	% Message when being under running process
    drawnow;
    
    display('Program has been launched...')
    
    tic
    [x, fval, exitFlag, output] = FMC(Option);
    toc
    elapseTime = ceil(toc/60*1e3)/1e3;                                      % Computation time in minutes
    set(hObject,'String','Start')
    set(handles.optStatus,'String',['Bingo !  ',num2str(elapseTime),' mins'])       	% Message when being under running process
    
    % Showing result
    if exitFlag < 0
        set(handles.exitFlag,'String','failed')
    else
        set(handles.exitFlag,'String','ok')
        set(handles.optOBJ,'String',num2str(fval))
        if isfield(output,'iterations')
            tempText = num2str(output.iterations);
        else
            tempText = 'N/A';
        end
        set(handles.nIter,'String',tempText);
        set(handles.funCount,'String',num2str(output.funcCount))
        
    end

else
    uiwait(errordlg('No input function and design domain found','Error'));
    return
end    


function exitFlag_Callback(hObject, eventdata, handles)
% hObject    handle to exitFlag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of exitFlag as text
%        str2double(get(hObject,'String')) returns contents of exitFlag as a double


% --- Executes during object creation, after setting all properties.
function exitFlag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to exitFlag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function optOBJ_Callback(hObject, eventdata, handles)
% hObject    handle to optOBJ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of optOBJ as text
%        str2double(get(hObject,'String')) returns contents of optOBJ as a double


% --- Executes during object creation, after setting all properties.
function optOBJ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to optOBJ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function funCount_Callback(hObject, eventdata, handles)
% hObject    handle to funCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of funCount as text
%        str2double(get(hObject,'String')) returns contents of funCount as a double


% --- Executes during object creation, after setting all properties.
function funCount_CreateFcn(hObject, eventdata, handles)
% hObject    handle to funCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nIter_Callback(hObject, eventdata, handles)
% hObject    handle to nIter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nIter as text
%        str2double(get(hObject,'String')) returns contents of nIter as a double


% --- Executes during object creation, after setting all properties.
function nIter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nIter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function multiStart_Callback(hObject, eventdata, handles)
% hObject    handle to multiStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of multiStart as text
%        str2double(get(hObject,'String')) returns contents of multiStart as a double


% --- Executes during object creation, after setting all properties.
function multiStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to multiStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxTrial_Callback(hObject, eventdata, handles)
% hObject    handle to maxTrial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxTrial as text
%        str2double(get(hObject,'String')) returns contents of maxTrial as a double


% --- Executes during object creation, after setting all properties.
function maxTrial_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxTrial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxIter_Callback(hObject, eventdata, handles)
% hObject    handle to maxIter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxIter as text
%        str2double(get(hObject,'String')) returns contents of maxIter as a double


% --- Executes during object creation, after setting all properties.
function maxIter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxIter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
