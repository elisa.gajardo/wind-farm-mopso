function [x,fval,exitFlag,output] = FMC(Option)

lb = Option.domain(:,1);                                                    % Lower bound
ub = Option.domain(:,2);                                                    % Upper bound

% Create empty inequality constraint if not existed
if ~isfield(Option,'A')
    Option.A = [];
end

if ~isfield(Option,'B')
    Option.B = [];
end

%-----------------------------------------------------------
% Options
%-----------------------------------------------------------
% Start with the default options
optsFMSC = optimset('fminsearch');                                          % Fminsearchcon option
optsFMC = optimoptions('fmincon');                                          % Fmincon options
if Option.parallel
    parPoolRun(2)                                                           % Start parallel computing
    optsFMSC = optimset(optsFMSC,'UseParallel',true);                       % Run in parallel
    optsFMC = optimoptions(optsFMC,'UseParallel',true);                     % Run in parallel
end

% Modify options setting
%-----------------------------------------------------------
optsFMSC = optimset(optsFMSC,'Display', 'off');
optsFMSC = optimset(optsFMSC,'MaxIter', Option.maxIter);
optsFMSC = optimset(optsFMSC,'MaxFunEvals', Option.maxFeval);
optsFMSC = optimset(optsFMSC,'TolFun', Option.tolFun);
optsFMSC = optimset(optsFMSC,'TolX', Option.tolX);
optsFMSC = optimset(optsFMSC,'FunValCheck', 'on');
if Option.display
    optsFMSC = optimset(optsFMSC,'PlotFcns', {@optimplotfunccount @optimplotfval});
end
optsFMSC = optimset(optsFMSC,'Algorithm', Option.algorithm);
%-----------------------------------------------------------

optsFMC = optimoptions(optsFMC,'Display', 'off');
optsFMC = optimoptions(optsFMC,'MaxIter', Option.maxIter);
optsFMC = optimoptions(optsFMC,'MaxFunEvals', Option.maxFeval);
optsFMC = optimoptions(optsFMC,'TolFun', Option.tolFun);
optsFMC = optimoptions(optsFMC,'TolX', Option.tolX);
% optsFMC = optimoptions(optsFMC,'TolCon', Option.tolCon);
optsFMC = optimoptions(optsFMC,'FunValCheck', 'on');
if Option.display
    optsFMC = optimoptions(optsFMC,'PlotFcns', {@optimplotfunccount @optimplotfval});
end
optsFMC = optimoptions(optsFMC,'Algorithm', Option.algorithm);
%-----------------------------------------------------------

xLast = [];                                                                 % Last place computeall was called
objective = [];                                                             % Use for objective at xLast
constraint = [];                                                            % Use for nonlinear inequality constraint

OBJfun = @myOBJ;                                                            % Objective function, nested below
CONSTRfun = @myCONSTR;                                                      % Constraint function, nested below

if strcmpi(Option.solver,'FMC')
    [x,fval,exitFlag,output] = fmincon(OBJfun,Option.x0,Option.A,Option.B,[],[],lb,ub,CONSTRfun,optsFMC);
elseif strcmpi(Option.solver,'FMSC')
    [x,fval,exitFlag,output] = fminsearchcon(OBJfun,Option.x0,lb,ub,Option.A,Option.B,CONSTRfun,optsFMSC);
else 
    error('Please choose solver either FMC or FMSC')
end

if Option.save
    save('Result_FMC','x','fval','exitFlag','output')
end

%-----------------------------------------------------------
% Tricky function to include both objective and constraint in one function
%-----------------------------------------------------------
    function y = myOBJ(x)
        if ~isequal(x,xLast)                                                % Check if computation is necessary
            [objective,constraint] = feval(Option.Fcn,x);
            xLast = x;
        end
        y = objective;
    end

    function [c,ceq] = myCONSTR(x)
        if ~isequal(x,xLast)                                                % Check if computation is necessary
            [objective,constraint] = feval(Option.Fcn,x);
            xLast = x;
        end
        c = constraint;
        ceq = [];
    end
end