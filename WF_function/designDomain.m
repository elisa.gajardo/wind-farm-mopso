% Function to get design variable space
function [domain, UserInput] = designDomain(UserInput)

%Create the particles domaine
% domain: matrix with N_variables lines and four columns with the following meaning:
%[lower_bound higher_bound discretisation_step do_screening]
%The discetization step is 0 if the variable is continuous
%the do_screening parameter must be 1 if a screening is expected and 0 ohterwise

%Load Number of variables
data = matfile('data_WF.mat');
N_variables = data.N_variables;%Number of non empty values

%Domain definition
domain = repmat([0 1 1 1],N_variables,1);
UserInput = [];