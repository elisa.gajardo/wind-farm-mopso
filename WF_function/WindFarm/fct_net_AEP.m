function [Net_AEP] = fct_net_AEP(total_energy, date_UTC)%date_UTC)
%UNTITLED3 Summary of this function goes here
%   compute the net AEP from the total energy produced throught the time
%   period

Net_AEP = total_energy*365*24/length(date_UTC);
end
