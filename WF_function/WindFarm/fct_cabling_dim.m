function [G] = fct_cabling_dim(G,sub_st_node)
%{
From fct_power_flow.m (François) + some modifications

Computes the power in each connection, selects dimmensions of cables and
adds the following properties to G.Edges.:
               G.Edges.P_nom
               G.Edges.cable_type : indexed in table cable_33 (loadcable_sections.mat)
               G.Edges.Nbcables : nb of cables per connection
               
               G.Edges.TotalPrice : M€  per cable (not used)

Inputs : - G : non directed graph representing the WF grid. Each WT is a node, 
               and so are the offshore & onshore substations
         - sub_st_node : Node where the offshore substation is located
                         (now, sub_st_node=1 always)

Outpust : - G : Updated graph. Edges have a new properties explained above
%}
 
nom_power=8*10^6; %Nominal power of each wt (8MW)
%% Find end nodes (WT that are on cable ends)

end_nodes = [];
nb_nodes = height(G.Nodes); % Number of nodes. height() returns nb of rows in a table
for k=2:nb_nodes
    out_nodes = outedges(G,k); %Neighbours of node k
    if length(out_nodes) == 1 % Only wt
        end_nodes = horzcat(end_nodes,k); %horizontal concatenation
    end
end

%% Initialization of computation

%Create a new attribute for the nodes. Nodes.Power is the power transiting
%through the WT, including the power it produces itself
G.Nodes.Power = zeros(nb_nodes,1)+nom_power*ones(nb_nodes,1);

%This other attribute is used to make sure power is not computed twice
G.Nodes.Visited = zeros(nb_nodes,1);

%% Computation of power in nodes
%For each end node, we compute the path from the node to the offshore
%substation. Then, Nodes.Power is updated by going along this path
for starting_node=end_nodes
    path = shortestpath(G,starting_node,sub_st_node);
    new_power = 0;
    %If a leaf is connected directly to a branch
    if G.Nodes.Visited(path(2)) == 1
        new_power = G.Nodes.Power(path(1));
    end
    %Go through the path nodes
    for edge_index = 2:length(path)
        prev_node = path(edge_index-1);
        node = path(edge_index);
        entering_power = G.Nodes.Power(prev_node); %Power coming in previous node
        current_power = G.Nodes.Power(node); %Current power in node
        if G.Nodes.Visited(node) == 0 %if first time in a node, no risk of redundance
            G.Nodes.Power(node) = current_power + entering_power;
            G.Nodes.Visited(node) = 1;
            if edge_index < length(path) %if it is not the last node of path
                if G.Nodes.Visited(path(edge_index+1)) == 1
                    new_power = current_power + entering_power;
                end
            end
        else %if not first time, we only add the power coming from added branch
            G.Nodes.Power(node) = current_power + new_power;
        end
    end
end

%% Compute power in edges
nb_edges = height(G.Edges);
%Add power attribute for edges, initialized to 0
G.Edges.P_nom = zeros(nb_edges,1);
%Power in an edge is the minimum power in the two linked nodes
for k=1:nb_edges
    edgenodes = G.Edges.EndNodes(k,:); %EndNodes is the two nodes that are linked by an edge
    power1 = G.Nodes.Power(edgenodes(1));
    power2 = G.Nodes.Power(edgenodes(2));
    G.Edges.P_nom(k) = min(power1,power2);
end


%% Cable Section (@33kV)
%load cable_sections cable_33
[data_cable_33,~] = fct_load_cable_data();
power = G.Edges.P_nom';
P_intervals = [data_cable_33.Power(:,2)' , 1e15]; %Power intervals in W (last element in case there exists section above limit)

[~, cable_type] = max(bsxfun(@le, power(:).', P_intervals(:)), [], 1); %Binary function search (max of lower or equal value)
G.Edges.Section_mm2 = zeros(nb_edges,1);
G.Edges.Cable_type=cable_type';
G.Edges.Nbcables=ones(height(G.Edges),1);

%For sections that surpass max MW, divide into various cables:
c=2;
while ~isempty(find(G.Edges.Cable_type==length(P_intervals), 1))
    sup=find(G.Edges.Cable_type==length(P_intervals));
    power=(G.Edges.P_nom(sup)/c)'; %Idea: divide the cable by c smaller cables
    
    [~, cable_type] = max(bsxfun(@le, power(:).', P_intervals(:)), [], 1); %Search for appropriate section
    solved = find(cable_type~=length(P_intervals));
    %Update cable sections
    G.Edges.Cable_type(sup(solved(:))) = cable_type(solved(:))';
    G.Edges.Nbcables(sup(solved(:))) = G.Edges.Nbcables(sup(solved(:))).*c;
    G.Edges.P_nom(sup(solved(:))) = power(solved(:));
    
    c=c+1;
end
G.Edges.Section_mm2 = data_cable_33.Section(G.Edges.Cable_type,2); %Cable section in mm2

%Add price to graph:
%G.Edges.TotalPrice=(G.Edges.Weight/1000).*
%cable_33.Price(G.Edges.Section).*G.Edges.CableQuant; %M€/cable

%% A
%G.Edges=renamevars(G.Edges,"Weight","Length");
G.Nodes.Visited=[]; %Remove 'Visited' variable


end

