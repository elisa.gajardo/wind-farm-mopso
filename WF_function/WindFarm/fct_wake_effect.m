function [S_grid] = fct_wake_effect(D,is_wt_grid,x_wt_index,y_wt_index,S_grid)
% Wake effect calculation (simplified)
% Input : - Wind direction D (radians)
%         - WT Grid & indexes, 
%         - Wind Grid (full or only on WT)
% Output : - Updated Wind Grid S_grid 
%            Wind speed only computed on actual WT positions
% Principle : the grid is fully rotated to compute everything as if wind
% was coming from north. Jensen model is used.

%Parameters of Jensen wake effect model
r_wt = 167/2; %[m] WT radius
Ct = 0.58; % WT thrust coefficient
alpha=0.04; %[] Expansion of the wake

%We are only interested in wind on WT positions
S_grid = S_grid.*is_wt_grid;

%Compute rotated coordinates of wind turbines
wt_rotated = [];
for i=1:length(y_wt_index)
    for j=1:length(x_wt_index)
        if is_wt_grid(i,j)==1
            [xwind,ywind] = fct_rotate_coordinates(x_wt_index(j),y_wt_index(i),D);
            wt_rotated = vertcat(wt_rotated,[xwind,ywind,i,j]);
        end
    end
end

%Sort wind turbines along wind direction (y in new coordinates)
wt_rotated = sortrows(wt_rotated,2);

%Compute the wake effect of each wind turbine
%For each WT, we go over all the others WT to evaluate its effect
for n=1:size(wt_rotated,1)
    j = wt_rotated(n,4);
    i = wt_rotated(n,3);
    S_wt = S_grid(i,j);
    for m=1:size(wt_rotated,1)
        xe_wind = wt_rotated(m,1);
        ye_wind = wt_rotated(m,2);
        Dx = xe_wind - wt_rotated(n,1);
        Dy = ye_wind - wt_rotated(n,2);
        r_w = r_wt + alpha*Dy;
        
        if Dy>0 && abs(Dx)<r_w
            S_grid(wt_rotated(m,3),wt_rotated(m,4)) = S_wt + S_wt*(sqrt(1-Ct)-1)*(r_wt/r_w)^2;
        end
    end
end
end



