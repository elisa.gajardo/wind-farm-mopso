function []=fct_cabling_plot(T, sub_st_coord, graph_grid, x_wt_index, y_wt_index,L)
%Plot: Figure1: oriented graph T of cables
%      Figure2: phisical representation of cable layout on wind farm

k=size(T,1);

%% Representation of graphs T{cl}

for cl=1:k
    figure()
    P=plot(T{cl});%,'LineWidth',T{cl}.Edges.P_nom.*T{cl}.Edges.Nbcables/20);
    highlight(P,1,'NodeColor','r')
    title("Graph of Cluster "+cl)
end

%% Representation of cabling on grid

x_wt_index=x_wt_index(1):L:x_wt_index(end);
y_wt_index=y_wt_index(1):L:y_wt_index(end);

set_plot()
axis equal
for cl=1:k
    [i,j]=find(graph_grid{cl});
    Coord=[x_wt_index(j)' y_wt_index(i)'];
    
    plot(Coord(:,1),Coord(:,2), 'o', 'SeriesIndex',cl)
    
    Adj_cables=adjacency(T{cl});
    for i = 2:size(Adj_cables,1)
        for j = 2:size(Adj_cables,2)
            if Adj_cables(i, j) == 1
                [a, b] = find(graph_grid{cl} == i);
                [c, d] = find(graph_grid{cl} == j);
                X = [x_wt_index(b), x_wt_index(d)];
                Y = [y_wt_index(a), y_wt_index(c)];
                plot(X,Y,'SeriesIndex',cl)
            end
        end
    end
    n = neighbors(T{cl},1); %Neighbs of sub_st_node(=1)
    for i=1:length(n)
        [a, b] = find(graph_grid{cl} == n(i));
        X = [x_wt_index(b), sub_st_coord(cl,1)];
        Y = [y_wt_index(a), sub_st_coord(cl,2)];
        plot(X,Y,'SeriesIndex',cl)
    end
    plot(sub_st_coord(cl,1),sub_st_coord(cl,2), '.r','MarkerSize',10)
end
end