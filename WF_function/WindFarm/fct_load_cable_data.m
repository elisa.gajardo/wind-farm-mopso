function [data_cable_33,data_cable_220] = fct_load_cable_data()

%% 33kV

% Power : W
Power33 = [0 13.5;
         13.5 16.1;
         16.1 18.2;
         18.2 20.3;
         20.3 22.8;
         22.8 26.2;
         26.2 30.4;
         30.4 33.9;
         33.9 38;
         38 40.4;
         40.4 44.3]*10^6;
     
% Section : nb of cores, section m2
Section33 = [3 70;
           3 95;
           3 120;
           3 150;
           3 185;
           3 240;
           3 300;
           3 400;
           3 500;
           3 630;
           3 800];      
Section33(:,2) = Section33(:,2)*10^(-6);

% Capacitance F/m
Capacitance33 = [0.18;
                 0.19;
                 0.21;
                 0.22;
                 0.24;
                 0.28;
                 0.29;
                 0.32;
                 0.32;
                 0.35;
                 0.38]*10^(-6)/10^3;
             
 % inductance H/m
 Inductance33 = [0.48;
                 0.44;
                 0.42;
                 0.41;
                 0.39;
                 0.38;
                 0.38;
                 0.35;
                 0.34;
                 0.32;
                 0.31]*10^(-3)/10^(3);
             
data_cable_33 = table();
data_cable_33.Power = Power33;
data_cable_33.Section = Section33;
data_cable_33.Capacitance = Capacitance33;
data_cable_33.Inductance = Inductance33;

%% 220kV

% Power : W
Power220 = [0 161;
         161 179;
         179 200;
         200 223;
         223 247;
         247 267;
         267 287]*10^6;
     
% Section : nb of cores, section m2
Section220 = [3 240;
           3 300;
           3 400;
           3 500;
           3 630;
           3 800;
           3 1000];      
Section220(:,2) = Section220(:,2)*10^(-6);

% Capacitance F/m
Capacitance220 = [0.14;
                 0.16;
                 0.18;
                 0.20;
                 0.21;
                 0.23;
                 0.25]*10^(-6)/10^3;
             
 % inductance H/m
 Inductance220 = [0.44;
                 0.42;
                 0.40;
                 0.38;
                 0.37;
                 0.36;
                 0.35]*10^(-3)/10^(3);
             
data_cable_220 = table();
data_cable_220.Power = Power220;
data_cable_220.Section = Section220;
data_cable_220.Capacitance = Capacitance220;
data_cable_220.Inductance = Inductance220;
end
