function [platform_cost] = fct_cost_platform(Snom)
% Compute platform cost from nominal apparent power
% Input : Snom : nominal apparent power (VA)
% Output : Platform cost (euros)

%% Parameters
alpha = 2.31*10^(6); % Fixed cost (euros)
beta = 80850*10^(-6); % Variable cost (euros/VA)

%% Calculation
platform_cost = alpha + beta * Snom;
end
