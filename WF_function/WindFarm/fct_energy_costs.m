function [Energy, Costs] = fct_energy_costs(is_wt_grid, wind_occurence, x_wt_index,y_wt_index,date_UTC, L,coord_onshore_subst,Pnom_WT)

nb_cluster =3;
n = 25; %Installation lifetime (years)
alpha = 0.05; %OPEX/CAPEX

L_we=1;
D=5*pi/3;

%% Optimize grid

[turbines_graph, graph_grid,coord_offshore_subst]=fct_cabling_cluster(nb_cluster,is_wt_grid,x_wt_index,y_wt_index,L,Pnom_WT);


%Compute characteristics of offshore to onshore links
[off2on_links] = fct_cabling_off2on(nb_cluster,turbines_graph,coord_offshore_subst,coord_onshore_subst);


%% Energy
% We have to compute the mean annual energy production
% Pas de temps : une heure
[net_TEP] = fct_net_TEP(wind_occurence, is_wt_grid, x_wt_index,y_wt_index,turbines_graph,graph_grid,nb_cluster,off2on_links,Pnom_WT);

[Net_AEP] = fct_net_AEP(net_TEP, date_UTC);

%% Costs
[CAPEX,OPEX] = fct_cost(turbines_graph,nb_cluster,alpha,off2on_links);


%% production+costs
r=0.08;

Costs = (CAPEX + OPEX)/(1+r);
Energy = Net_AEP/(1+r);
for k=2:n
    Costs = Costs + OPEX/(1+r)^k;
    Energy = Energy + Net_AEP/(1+r)^k;
end

end

