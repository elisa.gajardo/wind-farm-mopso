function [T, graph_grid, sub_st_coord]=fct_cabling_cluster(k,is_wt_grid,x_wt_index,y_wt_index,L,P_nom)
%{
MAIN fct for cable layout. Executes:
    1. Cluster grid into k(input) clusters
    2. For each cluster, calls fct_cabling() for minspantree and dimmensioning

Function finds an optimal cabling layout (clustering, minimum spanning
tree, etc) for a given wt grid as input

INPUTS: 
    k = nb of clusters (integer), is_wt_grid = wt grid (Matrix nxm),
    x_wt_index = x coordinates in meters (vector 1xm),
    y_wt_index = y coordinates in meters (vector 1xn).

OUTPUTS:
    T = (T{c}=cabling graph of cluster c) cell structure 1xk,
        Properties of T{c}.Edge:
            1. T{c}.Edge.Weight: length (m) from node1 to node2
            2. T{c}.Edge.P_nom: nominal power supported per cable
            3. T{c}.Edge.Section_mm2: mm2 section of cable (for losses)
            4. T{c}.Edge.Cable_type: row of database referencing cable properties
            5. T{c}.Edge.Nbcables: number of cables used per connection
    graph_grid = (graph_grid{c}=matrix grid of graph c) cell structure 1xk,
    sub_st = offshore substation coordinates (in meters) Matrix kx2.
%}



%% Identify each WT by number

is_wt_grid(find(isnan(is_wt_grid)))=0; %Replace NaN values with 0

%% Clustering: Kmeans()

%Distance matrix
[i,j]=find(is_wt_grid);
Coord=[x_wt_index(j)' y_wt_index(i)']; %Inputs of kmeans algorithm

%Kmeans:
%   INPUTS: Coord = 2column vector with wt coordinates (m), k = nb of clusters
%   OUTPUTS: idx = identifies each wt (row of Coord) with cluster (integer 1:k) , cent = center point of each cluster (coord in (m))
[idx,cent] = kmeans(Coord,k);

%% clustered_grid
% Number each wt wrt cluster
for t=1:length(idx)
    i=find(x_wt_index==Coord(t,1));
    j=find(y_wt_index==Coord(t,2));
    is_wt_grid(j,i)=idx(t); %Modify grid wrt clusters (wt belonging to cluster i -> wt=i)
end

%% Find substations
sub_st_coord=zeros(k,2);
   for c=1:k
       sub_st_coord(c,:) = fct_cabling_subst(cent(c,:), x_wt_index, y_wt_index,L); %Places subst at closest L/2 point wrt to center of cluster
       %sub_st_coord in [m]!
   end

%% Call fct_cabling()
graph_grid=cell(k,1);
T=cell(k,1);
sub_st_node=zeros(k,1);
price_cable=zeros(k,1);

for c=1:k
    aux=is_wt_grid;
    aux(find(aux~=c))=0;
    cluster_grid=aux; %Grid of cluster c: with wt=c if belongs to cluster, 0 otherwise
    
    
    [t,id_graph_grid,subst_n] = fct_cabling(cluster_grid, sub_st_coord(c,:), x_wt_index, y_wt_index,L,P_nom);
    
    %Revert resolution of grid (back to original size)
    id_graph_grid=id_graph_grid(:,1:2:end);
    id_graph_grid=id_graph_grid(1:2:end,:);
    
    graph_grid{c}=id_graph_grid; %Grid of graph c: id of graph on grid (subst=1 wt=2,3...n)
    T{c}=t; %Graph of cluster c
    sub_st_node(c)=subst_n;
    %
    %price_cable(c)=sum(T{c}.Edges.Price); %Without subst costs
    
end

end