function [transformer_cost] = fct_cost_transformer(Snom)
% Compute transformer cost from apparent nominal power
% Input : Snom : Apparent nominal power in transformer (VA)
% Output : Transformer cost (euros)

%% Parameters 
alpha = 0;
beta = 1.0334; 
gamma = 0.7513;

%% Calculation
transformer_cost = alpha + beta * Snom^(gamma);
end