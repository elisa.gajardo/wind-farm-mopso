function [grid] = fct_fill_matrix(base_matrix,values_vector)
%FCT_FILL_MATRIX Retuns a matrix with the values vector in the non Nan values of the base matrix
% INPUTS: base_matrix = Model to be filled, with NaN values in the not
%                        available positions and any other value in the others
%         values_vector: vector to be transformed in the matrix
% OUTPUT: grid: matrix in the same dimensions as base_matrix, with
%               values_vector in the non NaN positions

if(sum(~isnan(base_matrix),'all') == length(values_vector))
    grid = base_matrix;
    index_vector = 1;
    %grid dimensions
    m = size(base_matrix,1); 
    n = size(base_matrix,2);
    %Fill th matrix from top left to bottom right
    for lin = 1:m
        for col = 1:n
            if (~isnan(base_matrix(lin, col)))
                grid(lin,col) = values_vector(index_vector);
                index_vector = index_vector +1;
            end
        end
    end
else
    disp('Error, number of elements in the vector is not the one of available spots')
end
end

