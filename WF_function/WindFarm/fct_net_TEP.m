function [net_TEP] = fct_net_TEP(wind_occurence, is_wt_grid,x_wt_index,y_wt_index, turbines_graph,graph_grid,nb_cluster,off2on_links,Pnom_WT)
%FCT_STAT_BIN Summary of this function goes here
%  Function that uses the wind and the windfarm layout is_wt_grid to
%  calculate the total production of power of the farm, considering the
%  number of occurence of speed and direction
%   Before this function, you will need a timetable with date, wind speed,
%  direction.
% To get this use :
% import data from station 6 of ST Brieuc in variable stat6
% wind = timetable(stat6.date_UTC, fct_calculate_speed(stat6.U10,
% stat6.V10), fct_calculate_direction(stat6.U10,stat6.V10))
% wind.Properties.VariableNames = {'speed', 'direction'}

%Network parameters
cos_phi = 0.9;
U = 30*10^3; %[V]
%cable parameters
rho = 17*10^(-9);
N_wt = length(find(is_wt_grid==1));
P_nom = Pnom_WT*10^6; %[W] nominal wind turbine power
S_nom_transformer = N_wt*P_nom/cos_phi; %[VA] !!! later,change N_wt to the number of wt in each substation


%stock_prod = 0;
net_TEP=0;
speed_index_milieu=[0.5:1:15]; %le milieux des bins
direction_index_milieu=[15:30:360]; %le milieux des catégories
%total = length(speed_index_milieu)*length(direction_index_milieu);
cont = 0;
for i=1:size(speed_index_milieu,2)
    for j=1:size(direction_index_milieu,2)
        
        cont = cont+1;
%         fprintf('%.3f%% \n', cont*100/total); %print progress
%         
        S_grid=is_wt_grid*speed_index_milieu(i); %matrice uniforme à l'échelle de la ferme
        D=direction_index_milieu(j)*2*pi/360;
        S_grid = fct_wake_effect(D, is_wt_grid, x_wt_index, y_wt_index, S_grid);
        
        
        % Gross EP
        check_production=0;
        iwg_power = zeros(size(is_wt_grid));
        for k=1:size(is_wt_grid,2) %x
            for l=1:size(is_wt_grid,1)%y
                if is_wt_grid(l,k)==1
                    production = fct_power_curve_SG8_167DD(S_grid(l,k))*10^3;  %turbine production [W]
                    iwg_power(l,k) = production;
                    check_production = check_production + production;
                end
            end
        end
        
        %nb_cluster=3; %nb of clusters (can be changed)
        
        % ATTENTION: turbines_graph is NOT a graph (turbines_graph{1} IS a graph)
        % ATTENTION: iwg_nodes is NOT a matrix (iwg_nodes{1} IS a matrix)
        % sub_st: coordinates of k substations in METERS (matrix kx2)
        %------------------
        %N_wt = sub_st(end) - length(sub_st); %% number of wind turbines, CHANGE WHEN MORE SUBSTATIONS --> sub_st must be a vector, and so Nwt (turbines in each sustation)
        
        %% Losses
        
        P_sortie=0;
        for ind_cluster=1:nb_cluster
            cluster_graph = turbines_graph{ind_cluster};
            cluster_graph.Edges.cable_type=ones(height(cluster_graph.Edges),1);
            cluster_grid = graph_grid{ind_cluster};
            off2on_link = off2on_links(ind_cluster,:);
            [mpc_results] = fct_power_flow(cluster_graph,cluster_grid,iwg_power,off2on_link,Pnom_WT);
            P_offshore = -mpc_results.gen(end,2);
            if P_offshore>0
                P_sortie = P_sortie + P_offshore;
            end
        end
        
        net_TEP=net_TEP + P_sortie*wind_occurence(j,i)*10^6;
        
    end
end
end

