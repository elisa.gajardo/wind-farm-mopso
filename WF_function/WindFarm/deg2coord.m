function [pt_cart] = deg2coord(pt_ref,pt_deg)
%DEG2COORD Convert degrees into cartesian coordinates
%   Needs a reference point
%   Using project conventions
radius=6371;%[m] Earth mean radius

lat1=pt_ref(1)*pi/180;
lat2=pt_deg(1)*pi/180;
lon1=pt_ref(2)*pi/180;
lon2=pt_deg(2)*pi/180;

x=radius*abs((lon2-lon1)*cos((lat1+lat2)/2));
y=radius*abs(lat2-lat1);
pt_cart=[x,y];
end

