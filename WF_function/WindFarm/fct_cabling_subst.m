function [sub_st_coord] = fct_cabling_subst(c, x_wt_index, y_wt_index,L)
%Finds closest location at L/2 for a given point c

%% Round to closest value
x_subst_index=x_wt_index(1:end-1)+L/2; %only L/2 point are available for subst location
y_subst_index=y_wt_index(1:end-1)+L/2;

[~,indx] = min(abs(c(1)-x_subst_index)); %find index of min distance
[~,indy] = min(abs(c(2)-y_subst_index));

sub_st_coord=[x_subst_index(indx) y_subst_index(indy)];

end