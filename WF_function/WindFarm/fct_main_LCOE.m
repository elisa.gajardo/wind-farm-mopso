function [LCOE] = fct_main_LCOE(is_wt_grid, wind_occurence, x_wt_index,y_wt_index,date_UTC, L,coord_onshore_subst,Pnom_WT)

nb_cluster =3; %number of clusters
n = 25; %Installation lifetime (years)
alpha = 0.05; %OPEX/CAPEX coefficient

%% Optimize grid cabling

[turbines_graph, graph_grid,coord_offshore_subst]=fct_cabling_cluster(nb_cluster,is_wt_grid,x_wt_index,y_wt_index,L,Pnom_WT);

%Compute characteristics of offshore to onshore links
[off2on_links] = fct_cabling_off2on(nb_cluster,turbines_graph,coord_offshore_subst,coord_onshore_subst);

%% Energy production
% We have to compute the mean annual energy production
% Pas de temps : une heure
[net_TEP] = fct_net_TEP(wind_occurence, is_wt_grid, x_wt_index,y_wt_index,turbines_graph,graph_grid,nb_cluster,off2on_links,Pnom_WT);

[Net_AEP] = fct_net_AEP(net_TEP, date_UTC);

%% Costs estimation
[CAPEX,OPEX] = fct_cost(turbines_graph,nb_cluster,alpha,off2on_links);

%% LCOE calculation
[LCOE] = fct_get_LCOE(CAPEX, OPEX,Net_AEP,n); %[euros/MWh]

end

