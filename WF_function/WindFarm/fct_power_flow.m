function [mpc_results] = fct_power_flow(cluster_graph,cluster_grid,iwt_power,off2on_link,Pnom_WT)

%% Parameters
%Grid
U_grid = 33*10^3; %[V] Grid tension
U_o2o = 220*10^3; %[V] Offshore to onshore cable tension
f = 50; %[Hz] Network frequency
P_nom = Pnom_WT; %[MW] Nominal WT power
nb_nodes = height(cluster_graph.Nodes); % Number of nodes
nb_edges = height(cluster_graph.Edges); % Number of edges

%Cables
rho20 = 1.7241e-8; %[Ohm.m] thermal resistivity of copper at 20ºC
%Section, Capacitance, Inductance
%load data_cable_characteristics.m
%Data for only one cable...
[data_cable_33,data_cable_220] = fct_load_cable_data();

%Offshore transformer
r_transfo = 0.099; %[Ohm] Transformer resistance
L_transfo = 17.2*10^(-3); %[H] Transformer inductance

%pu bases for the grid : for MatPower calculations, all characteristics need to be in pu
Sbase_grid = 100*10^6; %[VA] Base apparent power
Ubase_grid = U_grid; %[V] Base voltage
rbase_grid = Ubase_grid^2/Sbase_grid; %[Ohm] Base resistance
xbase_grid = rbase_grid; %[Ohm] Base inductance
bbase_grid = Sbase_grid/(Ubase_grid^2); %[Ohm] Base susceptance

%pu bases for the off2on : for MatPower calculations, all characteristics need to be in pu
Sbase_o2o = 100*10^6; %[VA] Base apparent power
Ubase_o2o = U_o2o; %[V] Base voltage
rbase_o2o = Ubase_o2o^2/Sbase_o2o; %[Ohm] Base resistance
xbase_o2o = rbase_o2o; %[Ohm] Base inductance
bbase_o2o = Sbase_o2o/(Ubase_o2o^2); %[Ohm] Base susceptance

%% Building mpc file
% MatPower needs a special mpc file describing the grid to compute the
% power flow

% MatPower version
mpc.version = '2';

% MVA base
mpc.baseMVA = Sbase_grid*10^(-6); %[MVA] Base apparent power

% bus data ================================================================
% bus_i/type/Pd/Qd/Gs/Bs/area/Vm/Va/baseKV/zone/Vmax/Vmin
bus = zeros(nb_nodes+2,13);
% All WT : PQ nodes
for k=2:nb_nodes
    bus(k,:) = [k	1	0	0	0   0	1	1	0	U_grid/10^3	1	1.1	0.9];
end
% Offshore substation : 2 PQ nodes linked by a transformer
bus(1,:) = [1	1	0	0	0   0	1	1	0	U_grid/10^3	1	1.1	0.9];
bus(nb_nodes+1,:) = [nb_nodes+1	1	0	0	0   0	1	1	0	U_o2o/10^3	1	1.1	0.9];
% Onshore substation : 1 Vdelta node
bus(nb_nodes+2,:) = [nb_nodes+2	3	0	0	0   0	1	1	0	U_o2o/10^3	1	1.1	0.9];
mpc.bus = bus;

% generator data ==========================================================
%bus/Pg/Qg/Qmax/Qmin/Vg/mBase/status/Pmax/Pmin
gen = zeros(nb_nodes+2,10);
% All WT : P = producted active power, Q = 0
for k=2:nb_nodes
    [indx,indy] = find(cluster_grid==k);
    wt_power = iwt_power(indx,indy)*10^(-6);
    gen(k,:) = [k   wt_power 0  0  0  1   1   1  0   0];
end
% Offshore substation : 2 PQ nodes, P,Q=0
gen(1,:) = [1  0   0   0  0   1   1   1   0   0];
gen(nb_nodes+1,:) = [nb_nodes+1   0   0   0  0   1   1   1   0   0];
% Onshore substation
gen(nb_nodes+2,:) = [nb_nodes+2   0   0   0  0   1   1   1   0   0];
mpc.gen = gen;

% branch data =============================================================
%fbus/tbus/r/x/b/rateA/rateB/rateC/ratio/angle/status/angmin/angmax
branch = [];
% Offshore grid
for k=1:nb_edges
    for a=1:cluster_graph.Edges.Nbcables(k)
        % Get cable information
        EndNodes = cluster_graph.Edges.EndNodes(k,:);
        L = cluster_graph.Edges.Weight(k); %[m] Branch length
        cable_type = cluster_graph.Edges.Cable_type(k); %[] Cable type
        S = data_cable_33.Section(cable_type,1)*data_cable_33.Section(cable_type,2); %[m2] Cable section
        % Compute r,x,b
        % Beware : S,L,C in data_cable are for only one cable
        r = rho20*L/S; % Cable resistance
        x = (data_cable_33.Inductance(cable_type)*L*2*pi*f)/3; % Cable inductance
        b = (data_cable_33.Capacitance(cable_type)*L*2*pi*f)*3; % Cable susceptance
        % Convert r,x,b in pu
        r_pu = r/rbase_grid;
        x_pu = x/xbase_grid;
        b_pu = b/bbase_grid;
        branch = [branch; [EndNodes(1)  EndNodes(2)   r_pu   x_pu   b_pu    45   45   45   0   0   1   -360    360]];
    end
end
% Offshore transformer
% r_pu = r_transfo/rbase_o2o;
%x_pu = L_transfo*2*pi*f/xbase_o2o;
r_pu = 0.0022;
x_pu = 0.1200;
branch = [[1 nb_nodes+1 r_pu   x_pu   0    45   45   45   1   0   1   -360    360];branch];
% Offshore to onshore link
for a=1:off2on_link(5)
    % Compute cable characteristics
    L = off2on_link(1);
    cable_type = off2on_link(2);
    S = data_cable_220.Section(cable_type,1)*data_cable_220.Section(cable_type,2); %[m2] Cable section
    % Compute r,x,b
    % Beware : S,L,C in data_cable are for only one cable
    r = (rho20*L/S)/3; % Cable resistance
    x = (data_cable_220.Inductance(cable_type)*L*2*pi*f)/3; % Cable inductance
    b = (data_cable_220.Capacitance(cable_type)*L*2*pi*f)*3; % Cable susceptance
    % Convert r,x,b in pu
    r_pu = r/rbase_o2o;
    x_pu = x/xbase_o2o;
    b_pu = b/bbase_o2o;
    % Create branch
    branch = [branch ; [nb_nodes+1  nb_nodes+2   r_pu   x_pu   b_pu    45   45   45   0   0   1   -360    360]];
end
mpc.branch = branch;

%% Run power flow with MatPower
%case_info(mpc);
mpc_results = runpf(mpc,mpoption('verbose',0,'out.all',0));
%mpc_results = runpf(mpc)

end
