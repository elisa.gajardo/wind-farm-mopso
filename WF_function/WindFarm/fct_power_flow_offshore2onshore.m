function [P_substation,Q_substation,P_losses,Q_losses,mpc_results] = fct_power_flow_offshore2onshore(P_offshore,Q_offshore,L,cable_type)
% "Power flow" in the link between offshore substation and onshore substation
% (computes the tensions and active/reactive powers)
% Input : - P_offshore : active power in offshore substation
%         - L : connection length
%         - cable_type : connection cable type
% Output : - P_onshore, Q_onshore : active and reactive power
%            "produced" by the onshore substation
%          - P_losses, Q_losses : active and reactive power losses in the
%            connection
%          - mpc_results : MatPower structure with full results

%% A FAIRE


%% Parameters
%Grid
U_grid = 132*10^3; %[V] Grid tension
f = 50; %[Hz] Network frequency

%Cables
rho20 = 1.7241e-8; %[Ohm.m] thermal resistivity of copper at 20ºC
%Section, Inductance, Capacitance
%load data_cable_characteristics.m
%Data for only one cable...
data_cable = [1000*10^(-6) 0.35*10^(-6) 0.25*10^(-9)
              400*10^(-6)  0.40*10^(-6) 0.18*10^(-9)];

%pu bases : for MatPower calculations, all characteristics need to be in pu
Sbase = 1*10^6; %[VA] Base apparent power
Ubase = U_grid; %[V] Base voltage
rbase = Ubase^2/Sbase; %[Ohm] Base resistance
xbase = rbase; %[Ohm] Base inductance
bbase = Sbase/(Ubase^2); %[Ohm] Base susceptance

%% Building mpc file
% MatPower needs a special mpc file describing the grid to compute the
% power flow

% MatPower version
mpc.version = '2';

% MVA base
mpc.baseMVA = Sbase*10^(-6); %[MVA] Base apparent power

% bus data ================================================================
% bus_i/type/Pd/Qd/Gs/Bs/area/Vm/Va/baseKV/zone/Vmax/Vmin
mpc.bus = [1	2	0	0	0   0	1	1	0	U_grid/10^3	1	1.1	0.9;
    2	3	0	0	0   0	1	1	0	U_grid/10^3	1	1.1	0.9];
% Node 1 = offshore substation
% Node 2 = onshore substation

% generator data ==========================================================
%bus/Pg/Qg/Qmax/Qmin/Vg/mBase/status/Pmax/Pmin
mpc.gen = [1   P_offshore   Q_offshore   P_offshore -P_offshore   1   1   1   P_offshore   -P_offshore;
    2   0   0   P_offshore -P_offshore   1   1   1   0   0];

% branch data =============================================================
%fbus/tbus/r/x/b/rateA/rateB/rateC/ratio/angle/status/angmin/angmax

% Compute cable characteristics
S = data_cable(cable_type,1); %[m2] Cable section
% Compute r,x,b
% Beware : S,L,C in data_cable are for only one cable
r = (rho20*L/S)/3; % Cable resistance
x = (data_cable(cable_type,2)*L*2*pi*f)/3; % Cable inductance
b = (data_cable(cable_type,3)*L*2*pi*f)*3; % Cable susceptance
% Convert r,x,b in pu
r_pu = r/rbase;
x_pu = x/xbase;
b_pu = b/bbase;
% Create branch
mpc.branch = [1  2   r_pu   x_pu   b_pu    45   45   45   0   0   1   -360    360];

%% Run power flow with MatPower
mpc_results = runpf(mpc, mpoption('verbose', 0, 'out.all',0));

%mpc_results = runpf(mpc);

% Get losses
loss = sum(get_losses(mpc_results));
P_losses = real(loss);
Q_losses = imag(loss);

% Get power in offshore substation
P_substation = mpc_results.gen(2,2);
Q_substation = mpc_results.gen(2,3);

end