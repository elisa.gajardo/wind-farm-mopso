function [] = plot_wf(x_wt_index,y_wt_index,is_wt_grid,bornes,optimal_LCOE)
%PLOT_WF Summary of this function goes here
%   Detailed explanation goes here
%PLOT_WF_GRID plot the wind farm

dim_x = length(x_wt_index);
dim_y = length(y_wt_index);
figure(), hold on, grid on, box on
xlim([-500,dim_x*1000+1500])
ylim([-0.5,dim_y*1000+1500])
xlabel('Axe x [m]')
ylabel('Axe y [m]')
%axis square
xticks(500:2500:dim_x*1000+1000)
yticks(500:2500:dim_y*1000+1000)
title('Optimal WF');
set(gca,'YDir','reverse')
set(gca,'xaxislocation','top')
set(gca,'GridAlpha',1)
text(0,0,'Safety boundary','Color', 'blue')

subtitle(['LCOE = ',num2str(optimal_LCOE),'€/Wh'])

%plot wt

for i=1:dim_y
    for j=1:dim_x
        if is_wt_grid(i,j)==1
            plot(x_wt_index(j),y_wt_index(i),'or','Markerfacecolor','r','MarkerSize',5);
        end
    end
end


plot(bornes(:,1),bornes(:,2),'-b','Markerfacecolor','g','MarkerSize',5)
axis square

end


