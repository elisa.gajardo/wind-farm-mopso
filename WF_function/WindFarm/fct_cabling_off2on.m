function [off2on_links] = fct_cabling_off2on(nb_cluster,turbines_graph,coord_offshore_subst,coord_onshore_subst)

%% Parameters
P_nom = 8*10^6; %[W]

%% Computation
off2on_links = [];
for i=1:nb_cluster
    % Cable length
    L = sqrt((coord_offshore_subst(i,1)-coord_onshore_subst(1,1))^2+(coord_offshore_subst(i,2)-coord_onshore_subst(1,2))^2);
    % Cable section 
    nb_WT = height(turbines_graph{i}.Nodes)-1;
    power = nb_WT * P_nom; %[MVA]
    [cable_type,section_mm_2,nb_cables] = fct_cabling_off2on_dim(power);
    off2on_links = [off2on_links; L cable_type section_mm_2 nb_cables];
end

end
