function [P_substation,Q_substation,P_losses,Q_losses,mpc_results] = fct_power_flow_grid(cluster_graph,cluster_grid,iwt_power)
% Power flow in the grid (computes the tensions and active/reactive powers
% in each node and branch)
% Input : - cabling_graph : graph of only one cluster
%         - wt_power_list : list of active powers produced by each WT
% Output : - P_substation, Q_substation : active and reactive power
%            "produced" by the offshore substation
%          - P_losses, Q_losses : active and reactive power losses in the
%            grid
%          - mpc_results : MatPower structure with full results

%% A FAIRE
% Changer les entrées
% Inclure le bon cable type
% Inclure le bon load data

%% Parameters
%Grid
U_grid = 33*10^3; %[V] Grid tension
f = 50; %[Hz] Network frequency
P_nom = 8; %[MW] Nominal WT power
nb_nodes = height(cluster_graph.Nodes); % Number of nodes
nb_edges = height(cluster_graph.Edges); % Number of edges

%Cables
rho20 = 1.7241e-8; %[Ohm.m] thermal resistivity of copper at 20ºC
%Section, Capacitance, Inductance
%load data_cable_characteristics.m
%Data for only one cable...
[data_cable_33,data_cable_130] = fct_load_cable_data();

%pu bases : for MatPower calculations, all characteristics need to be in pu
Sbase = 1*10^6; %[VA] Base apparent power
Ubase = U_grid; %[V] Base voltage
rbase = Ubase^2/Sbase; %[Ohm] Base resistance
xbase = rbase; %[Ohm] Base inductance
bbase = Sbase/(Ubase^2); %[Ohm] Base susceptance

%% Building mpc file
% MatPower needs a special mpc file describing the grid to compute the
% power flow

% MatPower version
mpc.version = '2';

% MVA base
mpc.baseMVA = Sbase*10^(-6); %[MVA] Base apparent power

% bus data ================================================================
% bus_i/type/Pd/Qd/Gs/Bs/area/Vm/Va/baseKV/zone/Vmax/Vmin
bus = zeros(nb_nodes,13);
bus(1,:) = [1	3	0	0	0   0	1	1	0	U_grid/10^3	1	1.1	0.9];
for k=2:nb_nodes
    bus(k,:) = [k	2	0	0	0   0	1	1	0	U_grid/10^3	1	1.1	0.9];   
end
mpc.bus = bus;

% generator data ==========================================================
%bus/Pg/Qg/Qmax/Qmin/Vg/mBase/status/Pmax/Pmin
gen = zeros(nb_nodes,10);
gen(1,:) = [1   0   0   0  0   1   1   1   0   0];
for k=2:nb_nodes
    [indx,indy] = find(cluster_grid==k); 
    wt_power = iwt_power(indx,indy)*10^(-6);
    gen(k,:) = [k   wt_power   0   P_nom -P_nom   1   1   1   P_nom   -P_nom];
end
mpc.gen = gen;

% branch data =============================================================
%fbus/tbus/r/x/b/rateA/rateB/rateC/ratio/angle/status/angmin/angmax
branch = zeros(nb_edges,13);
for k=1:nb_edges
    % Get cable information
    EndNodes = cluster_graph.Edges.EndNodes(k,:);
    L = cluster_graph.Edges.Weight(k); %[m] Branch length
    cable_type = cluster_graph.Edges.cable_type(k); %[] Cable type
    S = data_cable_33.Section(cable_type,1)*data_cable_33.Section(cable_type,2); %[m2] Cable section
    % Compute r,x,b
    % Beware : S,L,C in data_cable are for only one cable
    r = rho20*L/S; % Cable resistance
    x = (data_cable_33.Inductance(cable_type)*L*2*pi*f)/3; % Cable inductance
    b = (data_cable_33.Capacitance(cable_type)*L*2*pi*f)*3; % Cable susceptance
    % Convert r,x,b in pu
    r_pu = r/rbase;
    x_pu = x/xbase;
    b_pu = b/bbase;
    branch(k,:) = [EndNodes(1)  EndNodes(2)   r_pu   x_pu   b_pu    45   45   45   0   0   1   -360    360];
end
mpc.branch = branch;

%% Run power flow with MatPower

mpc_results = runpf(mpc, mpoption('verbose', 0, 'out.all',0));

% Get losses
loss = sum(get_losses(mpc_results));
P_losses = real(loss);
Q_losses = imag(loss);

% Get power in offshore substation
P_substation = mpc_results.gen(1,2);
Q_substation = mpc_results.gen(1,3);

end