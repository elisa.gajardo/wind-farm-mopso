function [D] = fct_calculate_direction(U,V)
%FCT_CALCULATE_DIRECTION This function calulates the wind direction
%Input Arguments: U = the West to East wind speed component [m/s]
%                V the South to North wind speed component [m/s]
%Output Arguments: D = wind direction in degrees

    S = fct_calculate_speed(U, V);
    D = 180+(180/pi)*atan2(V./S, U./S);
    
end

