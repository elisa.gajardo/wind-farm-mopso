function [CAPEX,OPEX] = fct_cost(T,nb_cluster,alpha,off2on_links)
% Computes windfarm cost (WT, cables, platform, transformer, protection)
% There is no compensation equipment.
% Input : - T : Nondirected graph representing the grid, with attributes
%               Power and Weight (distance) for edges
%         - is_wt_grid : is_wt_grid, 1 on WT locations
% Outputs : - WF_cost : Windfarm cost (euros)

CAPEX=0;
for i=1:nb_cluster
    off2on_link = off2on_links(i,:);
    CAPEX = CAPEX + fct_cost_cluster(T{i},off2on_link);
end
OPEX = alpha*CAPEX;

end
