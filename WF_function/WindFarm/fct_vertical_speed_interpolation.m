function [S_vertical_int] = fct_vertical_speed_interpolation(S100, S10, z_hub)
%FCT_VERTICAL_SPEED_INTERPOLATION It calculates the vertical speed
%interpolation
%  Inputs: S100: wind speed at 100m of the ground [m/s]
%          S10: wind speed at 10m of the gound [m/s]
   %       z_hub: height to interpole the velocity [m]
alpha = log(S100./S10)/log(10);
S_vertical_int = S100.*(z_hub/100).^alpha; % [m/s] interpoled wind speed, normalized by S100 to eliminate C and at all the time instants

end

