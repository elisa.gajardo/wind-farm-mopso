function [cable_cost] = fct_cost_cable(Snom,Length,Vindex)
% Compute submarine cable cost from apparent nominal power and length
% Input : - Snom : Apparent nominal power transported by cable (VA)
%         - Length : Cable length (m)
%         - Vindex : Cable type index (integer)
% Output : Cable cost (euros)

%% Parameters 
% The costs depend on the voltage level.
% Voltage level index : - 1 : 33kV
%                       - 2 : 132kV
%                       - 3 : 220kV
% The costs are given by a set of parameters :
% > Transport and installation fixed cost :
C_SI = 117*10^3; %[euros/km] 
% > Voltage level dependent parameters :
submarine_costs_param = [47470.5 68838 4.1*10^6;
                         0.225*10^6 24139.5 1.66*10^6;
                         0.367*10^6 12705 1.16*10^6]; 
% Row : 33kV, 132kV, 220kV
% Column : alpha, beta, gamma

%% Calculation
param = submarine_costs_param(Vindex,:);
cable_cost = Length*10^(-3)*(C_SI + param(1) + param(2) * exp(param(3)*10^(-8)*Snom*10^(-6)));
end