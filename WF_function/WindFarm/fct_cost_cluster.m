function [WF_cost_cluster] = fct_cost_cluster(G,off2on_link)
% Computes windfarm cost (WT, cables, platform, transformer, protection)
% There is no compensation equipment.
% Input : - G : Nondirected graph representing the grid, with attributes
%               Power and Weight (distance) for edges
%         - substation_node : Offshore substation node index
%         - onshorestation_node : Onshore substation node index
%         - is_wt_grid_key : is_wt_grid where WT node indexes are on WT
%                            locations
%         - is_wt_grid : is_wt_grid, 1 on WT locations
% Outputs : - WF_cost_cluster : Windfarm cost per cluster(euros)


%% Parameters 
fp = 0.9; %[] Power coefficient cos(Phi)
P_WT_nominal = 8*10^(6); %[W] Nominal active power for one WT
WT_unitary_cost = 8*10^6; %[euros/WT] Unitary cost of a WT
WF_cost_cluster = 0; %[euros] Total wind farm cost

%% WT cost
n_WT = height(G.Nodes)-1; % Number of WT
WF_cost_cluster = WF_cost_cluster + n_WT*WT_unitary_cost;

%% Cable cost
[data_cable_33,data_cable_220] = fct_load_cable_data();
cable_cost = 0;
% Compute cost for each connection in the grid
nb_edges = height(G.Edges);
for k=1:nb_edges
    endnodes = G.Edges.EndNodes(k,:);
    L = G.Edges.Weight(k); % Connection length
    cable_type = G.Edges.Cable_type(k);
    S = data_cable_33.Power(cable_type,2);
    % In the farm : medium voltage 
    % Between substations : high voltage
    cable_cost = cable_cost + G.Edges.Nbcables(k)*fct_cost_cable(S,L,1);
end
S_off2on = data_cable_220.Power(off2on_link(2),2);
cable_cost = cable_cost + off2on_link(5)*fct_cost_cable(S_off2on,off2on_link(1),2);
WF_cost_cluster = WF_cost_cluster +cable_cost;

%% Platform & Transformer cost
Snom = n_WT*P_WT_nominal/fp; % Apparent power in transformer
WF_cost_cluster = WF_cost_cluster + fct_cost_platform(Snom) + fct_cost_transformer(Snom);

%% Protection units cost
% HV protection : for a connection from station to station
n_HT = off2on_link(5);
% MV protection : for a connection from station to WT
n_MT = sum(G.Edges.Nbcables(outedges(G,1)));
WF_cost_cluster = WF_cost_cluster + fct_cost_protection_unit(n_MT,n_HT);


end
