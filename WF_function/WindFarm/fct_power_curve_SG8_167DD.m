function [power]=fct_power_curve_SG8_167DD(wind)

% Wind turbine generator: https://www.thewindpower.net/turbine_en_1558_siemens-gamesa_sg-8.0-167-dd.php
% SG = synchronous machine, 8.0 MW, 167 m of diameter, DD = direct drived
% Data: estimated (L. Quéval 20200605)
wind_index = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 25, 26]; % wind speed [m/s]
power_index = [0, 4, 37, 125, 297, 581, 1005, 1596, 2383, 3393, 4654, 6195, 8043, 8043, 0]; % elec output power [kW]
  
power = interp1(wind_index, power_index, wind, 'nearest', 'extrap'); %interpolation
    
end