function [protection_unit_cost] = fct_cost_protection_unit(N_MT,N_HT)
% Compute the protection unit cost
% Input : - N_MT : Medium voltage breaker number
%         - N_HT : High voltage breaker number
% Output : Protection unit cost (euros)
% There must be one breaker by cable arriving on or leaving the
% substation. Their price depends on the voltage.

%% Parameters
C_PUMT = 0.364*10^(6); %[euros] Cost of one MT breaker
C_PUHT = 0.408*10^(6); %[euros] Cost of one HT breaker

%% Calculation
protection_unit_cost = N_HT*C_PUHT + N_MT*C_PUMT;
end