function [wind_occurence] = fct_get_wind(date_UTC, U10, V10, U100, V100)
%UNTITLED Summary of this function goes here
%   compute the wind occurence from a wind database

%% Parameters
z_hub = 96; %[m] Hub height

%% Calculation
S10 = fct_calculate_speed(U10,V10);
S100 = fct_calculate_speed(U100,V100);
S_pure = fct_vertical_speed_interpolation(S100, S10, z_hub);
direction = fct_calculate_direction(U100,V100);
date = date_UTC;
speed = S_pure;
wind = table(date,direction,speed);
[wind_occurence] = fct_stat_wind_binning(wind);
end

