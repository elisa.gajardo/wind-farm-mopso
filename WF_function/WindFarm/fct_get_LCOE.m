function [LCOE] = fct_get_LCOE(CAPEX, OPEX,Net_AEP,n)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

r=0.08;

Costs = (CAPEX + OPEX)/(1+r);
Energy = Net_AEP/(1+r);
for k=2:n
    Costs = Costs + OPEX/(1+r)^k;
    Energy = Energy + Net_AEP/(1+r)^k;
end

LCOE = Costs/Energy*10^6; %[euros/MWh]
end

