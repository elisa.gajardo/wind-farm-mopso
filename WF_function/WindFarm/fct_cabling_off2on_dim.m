function [cable_type,section_mm_2,nb_cables] = fct_cabling_off2on_dim(power)
%% Cable Section (@220kV)
%load cable_sections cable_220
[~,data_cable_220] = fct_load_cable_data();
P_intervals = [data_cable_220.Power(:,2)' , 1e15]; %Power intervals in W (last element in case there exists section above limit)

cable_type = min(find(power<=P_intervals)); % search (max of lower or equal value)


%For sections that surpass max MW, divide into various cables:
nb_cables=1;

while cable_type==length(P_intervals)
    nb_cables=nb_cables+1;
    power=power/nb_cables; %Idea: divide the cable by c smaller cables   
    cable_type = min(find(power<=P_intervals)); %Search for appropriate section
end
section_mm_2 = data_cable_220.Section(cable_type,:);
end
