function [Losses_transfo] = fct_losses_transformer(P_wtg,S_nom_transformer,U)
%FCT_TRANSFORMER_LOSSES Function to calculate the tranformer losses
%   Inputs: P_wtg: power injected by the wind turbine generator [W]
%           S_nom_transformer: Nominal apparent power in transformer (VA)
%           U: line-to-line voltage[V]
%   Output: Losses_transfo: calculated losses [W]

V = U/(3^(1/2));
pu = S_nom_transformer/U^2;
Rot = 200*pu; %Resistance [ohm]
rt = 0.005*pu; %internal resistance [ohm]

%No-load losses (iron losses)of the step-up transformer. It is constant when the voltage is well regulated.
PoT = V^2/Rot;

%Load losses of the step-up transformer.
PlT = ((P_wtg-PoT)^2)*rt/(V^2);

%Total losses
Losses_transfo = PlT+PoT;

end

