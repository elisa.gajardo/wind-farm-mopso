function [occurence] = fct_stat_wind_binning(wind)
% Function that uses the wind to
%  calculate the number of occurence of speed and direction
%   Before this function, you will need a timetable with date, wind speed,
%  direction.
% To get this use : 
% import data from station 6 of ST Brieuc in variable stat6
% wind = timetable(stat6.date_UTC, fct_calculate_speed(stat6.U10,
% stat6.V10), fct_calculate_direction(stat6.U10,stat6.V10));
% wind.Properties.VariableNames = {'speed', 'direction'};
%WindRose(wind.direction,wind.speed, ...
 %   "vWinds", [0 3 5 7 10 15], ...
  %  "lablegend", "Wind Speed in m/s");      %on trace la rose des vents pour 12 bins
%title('Wind Rose')
occurence=zeros(12,15);%i = ligne = directions ; j = colone = vitesse
bin_direction=discretize(wind.direction, 0:30:360); %renvoie l'index de la bin correspondante à la direction
bin_speed=discretize(wind.speed, 0:1:15); %renvoie l'index de la bin correspondante à la vitesse
for i=1:size(wind.direction,1)
    if not(isnan(bin_speed(i)))
        occurence(bin_direction(i), bin_speed(i))=occurence(bin_direction(i), bin_speed(i))+1;
    end
end
end

