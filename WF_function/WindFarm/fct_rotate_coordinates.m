function [xwind,ywind] = fct_rotate_coordinates(xgrid,ygrid,D)
% Rotates coordinates with an angle D
% Input : Wind direction D (radians), xgrid, ygrid
% Output : New coordinates xwind, ywind
xwind =  cos(D)*xgrid + sin(D)*ygrid;
ywind = -sin(D)*xgrid + cos(D)*ygrid;
end

