function [T,cluster_grid,sub_st_node] = fct_cabling(cluster_grid, sub_st_coord, x_wt_index, y_wt_index,L, nom_power)
%{ 
fct calculates minspantree graph T of a given cluster grid
Calls on fct_cabling_dim to dimmension graph (cable sections and P_nom)

grid CONVENTION: sub_station =  1
                 each wt has a unique number (2,3,4...,n)

substation (center of cluster)
%}


%TO DO: supprimer sub_st_node partout (not useful anymore)

%% Add resolution to grid (to place subst at L/2)
%In order build graph, place subst on grid. Resolution reverted at end of code
r=2;

z=zeros(size(cluster_grid,1),size(cluster_grid,2)*r-(r-1)); %add k*resolution to columns
z(:,1:r:end)=cluster_grid(:,1:1:end);
cluster_grid=z;

z=zeros(size(cluster_grid,1)*r-(r-1),size(cluster_grid,2));%add k*resolution to rows
z(1:r:end,:)=cluster_grid(1:1:end,:);
cluster_grid=z;

x_wt_index=x_wt_index(1):L/r:x_wt_index(end);
y_wt_index=y_wt_index(1):L/r:y_wt_index(end);


%% Identify each WT by number

Id=2:1:size(find(cluster_grid),1)+1;

cluster_grid(find(cluster_grid))=Id; %Assign each wt unique number
sub_st_node=1;%Id(end)+1;

%Substation node
i=x_wt_index==sub_st_coord(1);
j=y_wt_index==sub_st_coord(2);

cluster_grid(j,i)=sub_st_node;

%% Adjacency matrix and Weights

neighb=[];%TO DO: fix size
w=[];%TO DO: fix size
c=1;
[n_i,n_j]=size(cluster_grid);
for i = 1:n_i
    for j = 1:n_j
        if cluster_grid(i, j) > 0
            xa = x_wt_index(j);
            ya = y_wt_index(i);
            for r = 1:n_i
                for l = 1:n_j
                    if cluster_grid(r, l) > 0  && (i ~= r || j ~= l) % && grid(i ,j) < grid(k, l)
                        xb = x_wt_index(l);
                        yb = y_wt_index(r);
                        
                        mid_point=[min(xb,xa)+abs(xb-xa)/2, min(yb,ya)+abs(yb-ya)/2]; %coord mid point between two connections 
                        dist2subst=sqrt((sub_st_coord(1) - mid_point(1))^2 + (sub_st_coord(2) - mid_point(2))^2);
                        
                        weight = sqrt((xb - xa)^2 + (yb - ya)^2)+ dist2subst*1e-9; %Add a very small weight to penalise connections further from subst
                        
                  
                        neighb(c,:)=[cluster_grid(i,j) cluster_grid(r,l)];
                        w(c)=weight;
                        
                        c=c+1;
                    end
                end     
            end
        end
    end
end

%% Minimum spanning tree

G=graph(neighb(:,1),neighb(:,2),w);
T=minspantree(G,'Root', sub_st_node);

%Add cable section property to graph:
[T] = fct_cabling_dim(T,sub_st_node);

end
