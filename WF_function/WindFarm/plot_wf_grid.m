function [] = plot_wf_grid(x_wt_index,y_wt_index,is_wt_grid,bornes)
%PLOT_WF_GRID plot the wind farm

dim_x = length(x_wt_index);
dim_y = length(y_wt_index);
figure(), hold on, grid on, box on
xlim([-500,dim_x*1000+1500])
ylim([-0.5,dim_y*1000+1500])
xlabel('Axe x')
ylabel('Axe y')
%axis square
xticks(500:2500:dim_x*1000+1000)
yticks(500:2500:dim_y*1000+1000)
set(gca,'YDir','reverse')
set(gca,'xaxislocation','top')
set(gca,'GridAlpha',1)

%plot wt

for i=1:dim_y
    for j=1:dim_x
        if is_wt_grid(i,j)==0
            plot(x_wt_index(j),y_wt_index(i),'or','Markerfacecolor','r','MarkerSize',5);
        end
    end
end


plot(bornes(:,1),bornes(:,2),'o-g','Markerfacecolor','g','MarkerSize',5)
axis square
end

