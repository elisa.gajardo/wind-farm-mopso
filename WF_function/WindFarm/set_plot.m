function [] = set_plot()
%PLOT_CONV Summary of this function goes here
%   Detailed explanation goes here

figure(), hold on, grid on, box on
axis square

set(gca, 'Ydir', 'reverse') % invert ylabel, gca désigne la dernière figure active
set(gca, 'xaxislocation', 'top') % set xlabel on the top
set(gca, 'GridAlpha', 1) % estéthique

end

