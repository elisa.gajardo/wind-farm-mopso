function [S] = fct_calculate_speed(U, V)
%FCT_CALCULATE_SPEED this function calulates the wind direction
%Input Arguments: U = the West to East wind speed component [m/s]
%                V the South to North wind speed component [m/s]
%Output Arguments: S = wind speed, in the same units as U and V [m/s]
    S = sqrt(U.^2 + V.^2);
end

