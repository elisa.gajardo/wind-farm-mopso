function [x_wt_index,y_wt_index,is_wt_grid,boundaries_int,coord_onshore_subst] = fct_grid(boundaries_GPS,onshore_subst_deg)
%FCT_GRID This fuction returns:
%    x_wt_index vector with the mesh division in the x-direction, starting at 500m and with a pace of L
%    y_wt_index vector with the mesh division in the y-direction, starting at 500m and with a pace of L
%    is_wt_grid matrix which represents the wind farm, with:
%       0 = points with no tubine, but inside the boundaries
%       1 = wind turbine
%       nan = points outside the boundaries
%   boundaries matrix with the boundary location in the XY plane
  
% Input: boundaries_GPS = matrix with the coordinates of the delimitation points,
%                           in degrees, [[x_point1 y_point1]; 
%                                        [x_point2 y_point2]; .....]

global L;
%First, we define the grid, from the zone's format
%Corners definition
[x_max,~] = max(boundaries_GPS(:,1));
[x_min,~] = min(boundaries_GPS(:,1));
[y_max,~] = max(boundaries_GPS(:,2));
[y_min,~] = min(boundaries_GPS(:,2));

corners_deg = [[x_max y_min],
    [x_max y_max],
    [x_min y_min],
    [x_min y_max]];
corners = ones(size(corners_deg,1),2);

for i=1:1:length(corners_deg)
    %Conversion degree to coordinates
    temp=deg2coord(corners_deg(1,:),corners_deg(i,:));
    corners(i,1)=temp(1)*L;
    corners(i,2)=temp(2)*L;
end

boundaries_ext = ones(size(boundaries_GPS,1),2);
for i=1:1:length(boundaries_GPS)
    temp=deg2coord(corners_deg(1,:),boundaries_GPS(i,:));
    boundaries_ext(i,1)=temp(1)*L;
    boundaries_ext(i,2)=temp(2)*L;
end

coord_onshore_subst = deg2coord(corners_deg(1,:),onshore_subst_deg);


%Grid dimensions 
disty = (abs(corners(end,end))+L/2);
distx = (abs(corners(end,1))+L/2);

%Grid definitions
x_wt_index = (500:L:distx); %[m]
y_wt_index = (500:L:disty); %[m]

%Grid initialisation, only with zeros
is_wt_grid = zeros(length(y_wt_index),length(x_wt_index));

%Then, one can define an internal boundary, in order to prevent wind turbines 
%too close from the border. We left a margin of L/2
polygon_ext = polyshape(boundaries_ext(:,1), boundaries_ext(:,2));
polygon_int = polybuffer(polygon_ext,-L/2); 
% plot(polygon_ext); hold on;
% plot(polygon_int,'FaceColor','red');

[x_boundary_int,y_boundary_int] = boundary(polygon_int);
boundaries_int = [x_boundary_int y_boundary_int];
for i=1:1:length(y_wt_index) 
    for j=1:1:length(x_wt_index)
        if inpolygon(x_wt_index(j),y_wt_index(i),x_boundary_int,y_boundary_int)==false
                is_wt_grid(i,j) = nan; %Site non acceptable
        end
    end
end
%here, is matrix have the values 0 or nan

%INSERT CODE TO PLACE THE TURBINES
%for now, we place the turbines by hand
% is_wt_grid = [[nan nan nan nan nan nan nan nan nan nan nan nan nan nan];
%               [nan 0 0 nan nan nan nan nan nan nan nan nan nan nan];
%               [nan 1 0 1 0 1 0 nan nan nan nan nan nan nan];
%               [nan 0 1 0 1 0 1 0 0 0 nan nan nan nan];
%               [nan 1 0 1 0 1 0 1 0 1 nan nan nan nan];
%               [nan 0 1 0 1 0 1 0 1 0 nan nan nan nan];
%               [nan 1 0 1 0 1 0 1 0 1 nan nan nan nan];
%               [nan nan 0 0 1 3 1 0 1 0 nan nan nan nan];
%               [nan nan 0 1 0 1 0 1 0 1 0 nan nan nan];
%               [nan nan nan 0 1 0 1 0 1 0 0 nan nan nan];
%               [nan nan nan 1 0 1 0 1 0 1 0 nan nan nan];
%               [nan nan nan 0 1 0 1 3 1 0 1 nan nan nan];
%               [nan nan nan 1 0 1 0 1 0 1 0 nan nan nan];
%               [nan nan nan nan 1 0 1 0 1 0 1 nan nan nan];
%               [nan nan nan nan 0 1 0 1 0 1 0 1 nan nan];
%               [nan nan nan nan 0 0 1 0 1 0 0 0 nan nan];
%               [nan nan nan nan 0 1 0 1 3 1 0 1 nan nan];
%               [nan nan nan nan 0 0 0 0 0 0 0 0 nan nan];
%               [nan nan nan nan nan 1 0 1 0 1 0 1 nan nan];
%               [nan nan nan nan nan nan nan 0 0 0 0 0 nan nan];
%               [nan nan nan nan nan nan nan nan 0 1 0 nan nan nan];
%               [nan nan nan nan nan nan nan nan nan 0 0 0 nan nan];
%               [nan nan nan nan nan nan nan nan nan nan 0 nan nan nan];
%               [nan nan nan nan nan nan nan nan nan nan nan nan nan nan]];

%plot_wf_grid_2boundaries(x_wt_index,y_wt_index,is_wt_grid,boundaries_ext,boundaries_int)

end

