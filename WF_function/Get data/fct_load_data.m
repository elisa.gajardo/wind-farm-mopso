function [N_variables,is_wt_grid, wind_occurence, z_hub,L,x_wt_index, y_wt_index, boundaries_int, date_UTC, coord_onshore_subst,Nmax_WT, Pnom_WT] = fct_load_data()
%LOAD_DATA Summary of this function goes here
%   Detailed explanation goes here
    data = load('data_WF.mat');
    N_variables = data.N_variables;
    is_wt_grid = data.is_wt_grid;
    wind_occurence = data.wind_occurence;
    z_hub = data.z_hub;
    L = data.L;
    x_wt_index = data.x_wt_index;
    y_wt_index = data.y_wt_index;
    boundaries_int = data.boundaries_int;
    date_UTC = data.date_UTC;
    coord_onshore_subst = data.coord_onshore_subst;
    Nmax_WT = data.Nmax_WT;
    Pnom_WT = data.Pnom_WT;
end

