function [] = fct_create_data() 

global L;
global z_hub;
L=1000; %[m]
z_hub = 92; %[m] hub height
%year of interest, set to nan if goal is to get the whole data
year = 2010;
%GPS coordinates of the boundary
boundaries_GPS = [[48.93833 -2.61833],
    [48.91983 -2.61833],
    [48.89066 -2.61283],
    [48.85966 -2.59350],
    [48.82450 -2.57700],
    [48.79266 -2.56916],
    [48.77933 -2.57100],
    [48.73500 -2.48500],
    [48.76633 -2.44133],
    [48.91166 -2.47833],
    [48.93833 -2.61833]];

%GPS coordinates of the points where we have wind data
data_points= [[48.5 357.0-360],
    [48.5 357.25-360],
    [48.5 357.5-360],
    [48.75 357.0-360],
    [48.75 357.25-360],
    [48.75 357.5-360],
    [49.0 357.0-360],
    [49.0 357.25-360],
    [49.0 357.5-360]];

%Onshore substation : Doberie
onshore_subst_deg = [48.50164 -2.46421];

%% MAX WT
Pnom_WT = 8; %[MW]
Pmax_WF = 480; %[MW]
Nmax_WT = floor(Pmax_WF/Pnom_WT);
%% Grid definition

% nan = out of boundary
% 0 = available inside point
% 1 = wind turbine
% 2 = substation
%Construction of the grid

[x_wt_index,y_wt_index,is_wt_grid,boundaries_int,coord_onshore_subst] = fct_grid(boundaries_GPS,onshore_subst_deg);

N_variables = sum(~isnan(is_wt_grid),'all');


%% Wind speed calculation
% Interpolation of the speed and direction matrices in the whole farm, for all
% the dates of the defined year

%Import file with data of the point 6: 48.75N_357.50E
[date_UTC, U10, V10, U100, V100] = fct_importfile("data_StBrieuc\ERA5_StBrieuc_48.75N_357.50E.csv", [3, Inf], year);

% Wind binning
%wind occurence = matrix with the probabilities 
%i = ligne = directions ; j = colone = vitesse
[wind_occurence] = fct_get_wind(date_UTC, U10, V10, U100, V100);


save('data_WF.mat','is_wt_grid', 'wind_occurence','N_variables', 'L', 'z_hub', 'x_wt_index','y_wt_index','boundaries_int','date_UTC', 'coord_onshore_subst', 'Nmax_WT', 'Pnom_WT');
end 