%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Objective and constraint functions for MO optimization    	%
%                       --------------------                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [objective,constraint,misc] = myFun(Essaim,UserInput)
%-----------------------------------------
% Extract essaim for the effective use of parfor
% x = Essaim

%Paralel for
parfor i = 1:size(Essaim,2) %each column/Particle
    %----------------------------------------------
    % Data recovery
    %----------------------------------------------
    [~,is_wt_grid, wind_occurence, ~,L,x_wt_index, y_wt_index, ~,date_UTC, coord_onshore_subst,Nmax_WT, Pnom_WT] = fct_load_data();
    
    %----------------------------------------------
    % Objective calculation
    %----------------------------------------------
    Particle = Essaim(:,i) % get the column
    
    %Transform the particle vector in a matrix, respecting the NaN values
    grid = fct_fill_matrix(is_wt_grid,Particle);
       
    %Solve the model
    [LCOE] =fct_main_LCOE(grid, wind_occurence, x_wt_index,y_wt_index,date_UTC, L,coord_onshore_subst,Pnom_WT);
    N_wt = length(find(grid==1)); %count the number of wind turbines in the particle
    
    %Objective functions
    F1(i) = LCOE;
    F2(i) = F1(i);
	
    %Data to be saved in the miscelanius
    %K1(i) = turbines_graph;
%     K2(i) = graph_grid;
%     K3(i) = coord_offshore_subst;
    
    %Constraints
    g1 = N_wt-Nmax_WT; %N_wt inf or eq to __ (g <= 0, N_wt<=4, >> )
    constraint(1,i) = g1;
end

objective = [F1;F2]; % to be minimized
%constraint = zeros(1,size(x,2));

%misc = [K1];
misc = zeros(1,size(Essaim,2));
end