# WIND FARM LAYOUT OPTIMIZATION - PSO
### Version 03.06.2021


## Requirements
- Statistics and Machine Learning Toolbox
- Parallel Computing Toolbox


## Installation and Use
- Check the PDF tutorial (Manual_WF_PSO.pdf)


## Example visualization
- An optimization results example can be found in the folder 'Results Example (06.01.2021)'
- It was performed with 120 generations and 100 particles
- Copy the files and paste them in the folder MOPSO\Required files\Multiobjective optimization
- Proceed as shown from the item 16 of the manual forward
